'use strict';



function gsRmaRiparazioneCtrl($rootScope,$q,$scope,$stateParams,$interval,printTableMock,$location,
	$window,$uibModal, $log, $document,getGsGridRmaRiparazioneServices,$filter,_,gettextCatalog,
	$timeout, filterNumbers, filterDate,ActivityViewServices){
	
	/* global XLSX */

	$scope.stateParamsUI		= $stateParams;
	$scope.coloreUI 			= $scope.configGestSped[$scope.stateParamsUI.section].colorB;
	$scope.coloreColUI		= $scope.configGestSped[$scope.stateParamsUI.section].color;
	$scope.labelUI				= $scope.configGestSped[$scope.stateParamsUI.section].label;
	$scope.iconUI					= $scope.configGestSped[$scope.stateParamsUI.section].icon;
	$scope.icon2UI				= $scope.configGestSped[$scope.stateParamsUI.section].icon2;
	
	$scope.buttonFunctionGrid = {
		info		: {icon:'fa fa-info-circle'		,label:'Info Attivita\'', visible: false, classe : 'btn btn-default btn-xs'},
		edita 		: {icon:'fa fa-pencil'			,label:'Edita'			, visible: true, ngclick : '$ctrl.open()', classe : 'btn btn-primary btn-xs'},
		conferma 	: {icon:'fa fa-check'			,label:'Conferma'		, visible: false, classe : 'btn btn-primary btn-xs'},
		vedi 		: {icon:'fa fa-external-link'	,label:'Vedi Attivita'	, visible: true, classe : 'btn btn-info btn-xs'},
		preview 	: {icon:'fa fa fa-eye'			,label:'Preview File Ddt'	, visible: false, classe : 'btn btn-info btn-xs'},
		download 	: {icon:'fa fa fa-download'		,label:'Salva'			, visible: false, classe : 'btn btn-default btn-xs'}
	};
	
	//################### PROFILO AZIONI #########################
	$scope.profiloUI 		= $scope.$parent.profilo;
	
	$scope.magazziniUI 		= $scope.$parent.profilo.magazzini;
	$scope.joinMagazzini =  function(){
		if (!$scope.profiloUI.CM && !$scope.profiloUI.MG){
			$scope.showFunction = false;
		}
		// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
		var pCall = {};
		if(!$scope.profiloUI.admin){
			if ( angular.isUndefined(pCall.filter_MAGAZZINO_PARTENZA_in) ) {
				pCall.filter_MAGAZZINO_PARTENZA_in = _.pluck($scope.magazziniUI,'codMagSirti').toString();
			} 
		}
		return pCall;
		// jscs:enable requireCamelCaseOrUpperCaseIdentifiers
	};
	var sListaMag = $scope.joinMagazzini();
	//#################### FINE ##################################
	
	
	
	/* ricarico le statistiche */
	$timeout(function(){
		$scope.statisticheUI	= $scope.umStatisticheMenuCfg[$scope.stateParamsUI.section];
	});
	
	
	$scope.showFunction = true;
	
	/* gestione mostra nascondi colonne grid */
	$scope.selectAllColonneGrid = function(option) {
		_.each($scope.configGridUI, function(value,key) {
			
			if (value.enabled && key !== 'FUNCTION'){
				
				value.visible = option==='TUTTI' ? true:false;
			}
		});
	};		

	$scope.configGridUI		= {
		FUNCTION: {
		    divider: false,
		    name: 'FUNCTION',
		    label: '',
		    visible: true,
		    orderable: false,
		    filtered: false,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: 'fa-cogs',
		    enabled: false,
			enableFilter : false,
		    type:'string'
		  },
		  SOCIETA: {
		    divider: false,
		    name: 'SOCIETA',
		    label: 'SOCIETA',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '130px',
		    icon: '',
		    enabled: true,
			enableFilter : true,
		    type:'string'
		  },
		  LOCAZIONE: {
		    divider: false,
		    name: 'LOCAZIONE',
		    label: 'UBICAZIONE',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '130px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'string'
		  },
		  ID_ATTIVITA: {
		    divider: false,
		    name: 'ID_ATTIVITA',
		    label: 'ID ATTIVITA',
		    visible: true,
		    orderable: true,
		    filtered: false,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '130px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'number'
		  },
		  STATO: {
		    divider: false,
		    name: 'STATO',
		    label: 'STATO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '130px',
		    icon: '',
		    enabled: true,
			enableFilter : true,
		    type:'string'
		  },
		  DATA_APERTURA: {
		    divider: false,
		    name: 'DATA_APERTURA',
		    label: 'DATA APERTURA',
		    visible: true,
		    orderable: true,
		    filtered: false,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'date'
		  },
		  ID_RMA_RIPARATORE: {
		    divider: false,
		    name: 'ID_RMA RIPARATORE',
		    label: 'ID RMA RIP.',
		    visible: true,
		    orderable: true,
		    filtered: false,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '110px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'string'
		  },
		  ID_SIRTI: {
		    divider: false,
		    name: 'ID_SIRTI',
		    label: 'ID SIRTI',
		    visible: true,
		    orderable: true,
		    filtered: false,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'number'
		  },
		  CODICE_SIRTI: {
		    divider: false,
		    name: 'CODICE_SIRTI',
		    label: 'COD MATERIALE',
		    visible: true,
		    orderable: true,
		    filtered: false,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'number'
		  },
		  PART_NUMBER: {
		    divider: false,
		    name: 'PART_NUMBER',
		    label: 'PART NUMBER',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'string'
		  },
		  SAP_CODE: {
		    divider: false,
		    name: 'SAP_CODE',
		    label: 'SAP CODE',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '110px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'number'
		  },
		  SERIAL_NUMBER: {
		    divider: false,
		    name: 'SERIAL_NUMBER',
		    label: 'SERIAL NUMBER',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'string'
		  },
		  MAGAZZINO_PARTENZA: {
		    divider: true,
		    name: 'MAGAZZINO_PARTENZA',
		    label: 'MAG. PARTENZA',
		    visible: false,
		    orderable: true,
		    filtered: false,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
			enableFilter : true,
		    type:'string'
		  },
		  RIPARATORE: {
		    divider: false,
		    name: 'RIPARATORE',
		    label: 'RIPARATORE',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '450px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'string'
		  },
		  INDIRIZZO_RIPARATORE: {
		    divider: false,
		    name: 'INDIRIZZO_RIPARATORE',
		    label: 'IND. RIPARATORE',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '350px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'string'
		  },
		  RESTITUZIONE_MATERIALE: {
		    divider: false,
		    name: 'RESTITUZIONE_MATERIALE',
		    label: 'REST. MAT.',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'string'
		  },
		  PROGETTO: {
		    divider: false,
		    name: 'PROGETTO',
		    label: 'PROGETTO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'string'
		  },
		  SOTTO_PROGETTO: {
		    divider: false,
		    name: 'SOTTO_PROGETTO',
		    label: 'SOTTO PROGETTO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'string'
		  },
		  PROPRIETA: {
		    divider: false,
		    name: 'PROPRIETA',
		    label: 'PROPRIETA',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'string'
		  },
		  CLIENTE_FINALE: {
		    divider: false,
		    name: 'CLIENTE_FINALE',
		    label: 'CLIENTE FINALE',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '250px',
		    icon: '',
		    enabled: false,
			enableFilter : true,
		    type:'string'
		  },			
		  QUANTITA: {
		    divider: false,
		    name: 'QUANTITA',
		    label: 'QTA',
		    visible: false,
		    orderable: true,
		    filtered: false,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
			enableFilter : true,
		    type:'number'
		  },
		  CLASSIFICAZIONE: {
		    divider: true,
		    name: 'CLASSIFICAZIONE',
		    label: 'CLASSIFICAZIONE',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
			enableFilter : true,
		    type:'string'
		  },

		  IND_MAGAZZINO_PARTENZA: {
		    divider: true,
		    name: 'IND_MAGAZZINO_PARTENZA',
		    label: 'IND MAG. PART.',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
			enableFilter : true,
		    type:'string'
		  },
		  ID_CATALOGO: {
		    divider: true,
		    name: 'ID_CATALOGO',
		    label: 'ID CATALOGO',
		    visible: false,
		    orderable: true,
		    filtered: false,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
			enableFilter : true,
		    type:'number'
		  },
		  TIPO: {
		    divider: true,
		    name: 'TIPO',
		    label: 'TIPO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
			enableFilter : true,
		    type:'string'
		  },
		  STATO_ATTIVITA_LC: {
		    divider: true,
		    name: 'STATO_ATTIVITA_LC',
		    label: 'STATO ATTIVITA LC',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configGestSped[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
			enableFilter : true,
		    type:'string'
		  }


	};


	/* configurazione della modale invocata dalla UI */
	$scope.configModalUI =	{
		SELECTED: {
		    name: 'SELECTED',
		    label: '',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    icon:'fa fa-cogs',
		    action: '',
		    classe:{'width': '100px', 'min-width': '100px', 'max-width': '100px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  SOCIETA: {
		    name: 'SOCIETA',
		    label: 'SOCIETA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
			classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
			visibleCol: true,
			mostraNascondi: false
		  },
		  ID_ATTIVITA: {
		    name: 'ID_ATTIVITA',
		    label: 'ID ATTIVITA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  STATO: {
		    name: 'STATO',
		    label: 'STATO',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  DATA_APERTURA: {
		    name: 'DATA_APERTURA',
		    label: 'DATA APERTURA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
			LOCAZIONE: {
				name: 'LOCAZIONE',
				label: 'UBICAZIONE',
				visibleInSummary: true,
				visibleInVerify: false,
				visibleInAction: false,
				action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
			},
		  ID_RMA_RIPARATORE: {
		    name: 'ID_RMA_RIPARATORE',
		    label: 'ID RMA RIPARATORE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  ID_SIRTI: {
		    name: 'ID_SIRTI',
		    label: 'ID SIRTI',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  CODICE_SIRTI: {
		    name: 'CODICE_SIRTI',
		    label: 'CODICE SIRTI',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  PART_NUMBER: {
		    name: 'PART_NUMBER',
		    label: 'PART NUMBER',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  SAP_CODE: {
		    name: 'SAP_CODE',
		    label: 'SAP CODE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  SERIAL_NUMBER: {
		    name: 'SERIAL_NUMBER',
		    label: 'SERIAL NUMBER',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '180px', 'min-width': '180px', 'max-width': '180px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  RIPARATORE: {
		    name: 'RIPARATORE',
		    label: 'RIPARATORE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '300px', 'min-width': '300px', 'max-width': '300px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  INDIRIZZO_RIPARATORE: {
		    name: 'INDIRIZZO_RIPARATORE',
		    label: 'IND. RIPARATORE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '300px', 'min-width': '300px', 'max-width': '300px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  MAGAZZINO_PARTENZA: {
		    name: 'MAGAZZINO_PARTENZA',
		    label: 'MAG. PARTENZA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  RESTITUZIONE_MATERIALE: {
		    name: 'RESTITUZIONE_MATERIALE',
		    label: 'RESTITUZIONE MATERIALE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '200px', 'min-width': '200px', 'max-width': '200px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  PROGETTO: {
		    name: 'PROGETTO',
		    label: 'PROGETTO',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  SOTTO_PROGETTO: {
		    name: 'SOTTO_PROGETTO',
		    label: 'SOTTO_PROGETTO',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  PROPRIETA: {
		    name: 'PROPRIETA',
		    label: 'PROPRIETA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  CLIENTE_FINALE: {
		    name: 'CLIENTE_FINALE',
		    label: 'CLIENTE_FINALE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  MATERIALE_DA_SPEDIRE: {
		    name: 'MATERIALE_DA_SPEDIRE',
		    label: 'Materiale Da Spedire',
		    visibleInSummary: false,
		    visibleInVerify: false,
		    visibleInAction: true,
		    action: 'MATERIALE_DA_SPEDIRE',
		    isMassiva: true,
		    statoAZ: 'APPROVATO'
		  },
		  FLAG_MOD_SPED_AZ_MAT_DA_SPED: {
		    name: 'FLAG_MOD_SPED_AZ_MAT_DA_SPED',
		    label: 'MODALITA\' SPEDIZIONE',
		    visibleInSummary: false,
		    visibleInVerify: true,
		    visibleInAction: false,
		    action: 'MATERIALE_DA_SPEDIRE',
		    typeEdit: 'SELECT',
		    notNull: true
		  },
		  RUOLO_SQUADRA_AZ_MAT_DA_SPED: {
		    name: 'RUOLO_SQUADRA_AZ_MAT_DA_SPED',
		    label: 'RUOLO SQUADRA',
		    visibleInSummary: false,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: 'MATERIALE_DA_SPEDIRE',
		    typeEdit: 'TEXT',
		    notNull: false
		  },
		  DDT_AZ_MAT_DA_SPED: {
		    name: 'DDT_AZ_MAT_DA_SPED',
		    label: 'DDT',
		    visibleInSummary: false,
		    visibleInVerify: true,
		    visibleInAction: false,
		    action: 'MATERIALE_DA_SPEDIRE',
		    typeEdit: 'TEXT',
		    notNull: true
		  },
		  DATA_DDT_AZ_MAT_DA_SPED: {
		    name: 'DATA_DDT_AZ_MAT_DA_SPED',
		    label: 'DATA DDT',
		    visibleInSummary: false,
		    visibleInVerify: true,
		    visibleInAction: false,
		    action: 'MATERIALE_DA_SPEDIRE',
		    typeEdit: 'DATE',
		    notNull: true
		  },
		  ID_LOTTO_AZ_MAT_DA_SPED: {
		    name: 'ID_LOTTO_AZ_MAT_DA_SPED',
		    label: 'ID LOTTO',
		    visibleInSummary: false,
		    visibleInVerify: true,
		    visibleInAction: false,
		    action: 'MATERIALE_DA_SPEDIRE',
		    typeEdit: 'TEXT',
		    notNull: true
		  },
		  LOCAZIONE_AZ_MAT_DA_SPED: {
		    name: 'LOCAZIONE_AZ_MAT_DA_SPED',
		    label: 'UBICAZIONE',
		    visibleInSummary: false,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: 'MATERIALE_DA_SPEDIRE',
		    typeEdit: 'TEXT',
		    notNull: false
		  },
		  CAMBIO_RIPARATORE: {
		    name: 'CAMBIO_RIPARATORE',
		    label: 'Cambio Riparatore',
		    visibleInSummary: false,
		    visibleInVerify: false,
		    visibleInAction: $scope.profiloUI.admin ? true : false,
		    action: 'CAMBIO_RIPARATORE',
		    isMassiva: true,
		    statoAZ: 'APERTA'
		  },
		  RIPARATORE_AZ_CAMBIO_RIP: {
		    name: 'RIPARATORE',
		    label: 'RIPARATORE',
		    visibleInSummary: false,
		    visibleInVerify: true,
		    visibleInAction: false,
		    action: 'CAMBIO_RIPARATORE',
		    typeEdit: 'TEXT',
		    notNull: true
		  },
		  INDIRIZZO_RIPARATORE_AZ_CAMBIO_RIP: {
		    name: 'INDIRIZZO_RIPARATORE_AZ_CAMBIO_RIP',
		    label: 'INDIRIZZO RIPARATORE',
		    visibleInSummary: false,
		    visibleInVerify: true,
		    visibleInAction: false,
		    action: 'CAMBIO_RIPARATORE',
		    typeEdit: 'TEXT',
		    notNull: true
		  },
		  APPROVATO: {
		    name: 'APPROVATO',
		    label: 'Rma Accettato',
		    visibleInSummary: false,
		    visibleInVerify: false,
		    visibleInAction: true,
		    action: 'APPROVATO',
		    isMassiva: false,
		    statoAZ: 'APERTA'
		  },
		  ID_RMA_RIPARATORE_AZ_APPROVATO: {
		    name: 'ID_RMA_RIPARATORE_AZ_APPROVATO',
		    label: 'ID RMA RIPARATORE',
		    visibleInSummary: false,
		    visibleInVerify: true,
		    visibleInAction: false,
		    action: 'APPROVATO',
		    typeEdit: 'TEXT',
		    notNull: true
		  }
	};

	$scope.options = {
			orderBy : ['ID_ATTIVITA'],
			global : '',
			searchSingleColumn : true
	};
	$scope.currentPage  = 1;
	$scope.itemsPerPage = 10;
	$scope.orderByField = $scope.configGridUI[$scope.options.orderBy[0]].name;
	$scope.reverseSort  = false;
	$scope.maxSizePager = 6;
	$scope.nuovoNumero = 10;
	$scope.mostraLoaderGrid = true;
	
	// composto data header e data
	$scope.objectDati = {
			header:$scope.configGridUI,
			data:[]
	};
	
	
	//##################################### ATTIVAZIONE FILTRI ###################################//
	$scope.filterText = {}; // mappa chiave/valore con campo/valore da filtrare
	$scope.filterOperator = {}; // mappa chiave/valore con campo/operatore di filtro da applicare
	$scope.sortDirection = {}; // mappa chiave/valore con campo/direzione di ordinamento
	$scope.sortOrderArray = []; // array con l'elenco ordinato dei campi da ordinare
	$scope.sortOrderMap = {}; // mappa chiave/valore con i campi/posizione da ordinare. parallelo a $scope.sortOrderArray ma serve per essere passato
								// alla direttiva sirti-column-sort-and-filter nell'attributo sort-position in modo che non debba essere calcolato
	
	$scope.filterOrOrderApplied = false;
	
	function isFilterOrOrderApplied() {
		var isFiltered = false;
		var isSorted = false;
		_.each($scope.filterOperator, function(value) {
			if(!_.isUndefined(value)) {
				isFiltered = true;
			}
		});
		_.each($scope.sortOrderMap, function(value) {
			if(!_.isUndefined(value)) {
				isSorted = true;
			}
		});
		return isFiltered || isSorted;
	}
	
	$scope.$watch('filterOperator', function() {
		$scope.filterOrOrderApplied = isFilterOrOrderApplied();
	});
	
	$scope.$watch('sortOrderMap', function() {
		$scope.filterOrOrderApplied = isFilterOrOrderApplied();
	});
	
	// funzione che recepisce l'applica della direttiva sirti-column-sort-and-filter
	$scope.applyFilterAndSort = function(field, filterText, filterOperator, sortDirection) {
		if(field === 'id' && filterText && !filterText.match(/^\d+$/)) {
			$scope.alert.warning(gettextCatalog.getString('Activity Id must be a number'));
			return;
		}
	
		$scope.filterText[field] = filterText;
		$scope.filterOperator[field] = filterOperator;
		$scope.sortDirection[field] = sortDirection;
		if($scope.sortDirection[field]) {
			// la direzione di sort del campo è valorizzata e la aggiungo all'array se non ancora presente
			if(!_.contains($scope.sortOrderArray, field)) {
				$scope.sortOrderArray.push(field);
			}
		} else {
			// ...altrimenti elimino il campo dall'ordinamento
			$scope.sortOrderArray = _.without($scope.sortOrderArray, field);
		}
		// costruisco la mappa a partire dall'array di ordinamento (vedi nota sull'oggetto $scope.sortOrderMap)
		$scope.sortOrderMap = {};
		var i = 1;
		_.each($scope.sortOrderArray, function(value) {
			$scope.sortOrderMap[value] = i++;
		});
		// invoco il reload della tabella
		$scope.getGrid();
	};
	
	
	// pulisco filtri
	$scope.clearFiltersAndOrdering = function() {
		$scope.filterText = {};
		$scope.filterOperator = {};
		$scope.sortDirection = {};
		$scope.sortOrderArray = [];
		$scope.sortOrderMap = {};
		$scope.options.global = '';
		// invoco il reload della tabella
		$scope.getGrid();
	};
	
	//###################################### FINE PREPARAZIONE RICERCA #####################################//
	
	$scope.getGrid = function() {
		//var withFilter = [];//['permitsAreaId'];
		//var withOutSorter = [];
		//var withAutocompleate = [];//['macroTaskSubCategory','macroTaskCategory'];
		//var hidden = [];//['Pfp','Pop','projectId','customerId','contractId','buildingId','macroTaskType'];
		var oldTemp = $scope.data || [];
		var temp = [];
		
		$scope.mostraLoaderGrid = true;
	

	
			// #################### DEFAULT IN BASE PROFILO AZIONI ###################
		var pCall = $scope.joinMagazzini();
		// #################### DEFAULT IN BASE PROFILO AZIONI  FINE###################

		// parametri in URI di ricerca su BKEND		
		var params = {
				sort:[],
				pCall : pCall
		};
		
		//################################# POPOLAMENTO FILTRI ##################################//
		
		var localFilter = {};
		// definisco i parametri di filtro (testo e operatore)
		_.each($scope.filterOperator, function(value, key) {
			if($scope.filterOperator[key] && $scope.filterText[key]) {
				// solo in questo caso setto il parametro dello skip a 0 
				// in modo da avere sempre la ripartenza dallo 0esimo
				//params.skip = 0;
				params[key + '_' + $scope.filterOperator[key]] = $scope.filterText[key];
				// popolo i filtri locali
				localFilter[key] = $scope.filterText[key];

				// remap per far funzionare l'equal
				if($scope.filterOperator[key] === 'equal'){$scope.filterOperator[key] = 'equals';}
				// aggiungo i parametri di ricerca a pCall
				pCall['filter_'+ key + '_' + $scope.filterOperator[key]] = $scope.filterText[key];
				
				if(key === 'status') {
					// per lo status sostituisco gli spazi con _ secondo la convenzione di chiamare gli stati senza spazi e con le parole divise da _
					params[key + '_' + $scope.filterOperator[key]] = params[key + '_' + $scope.filterOperator[key]].replace(/ /g, '_');
				}
			}
		});
	
		// definisco l'ordine di sort
		_.each($scope.sortOrderArray, function(key) {
			if($scope.sortDirection[key]) {
				params.sort.push(($scope.sortDirection[key] === 'ASC' ? '' : '-') + key);
			}
		});
	
		// se non c'è alcun ordinamento ordino per id DESC
		// agisco in questo punto per evitare che la direttiva sirti-column-sort-and-filter evidenzi tale ordinamento
		if(params.sort.length === 0) {	
			params.sort = $scope.options.filterBy;
		}
		// ############################################ FINE POPOLAMENTO ########################################// 
		getGsGridRmaRiparazioneServices.getData(pCall).$promise.then(function(result) {
			temp = result.data.results;
			$scope.totalItems = result.data.count;
			
			$scope.objectDati.data = temp; 
			$scope.objectDati = filterNumbers(filterDate($scope.objectDati));

			// ###################################### FILTRO LOCALE ATTIVO ##########################//
			// FIXME filtri locali in caso attivati i filtri sui campi 
			$scope.objectDati.data = $filter('orderBy')($scope.objectDati.data = $filter('filter')((temp || oldTemp),localFilter),params.sort);
			// ###################################### FILTRO LOCALE ATTIVO ##########################//
			$scope.data = $scope.objectDati.data;
			
			$scope.data = _.each($scope.objectDati.data,function(row){
				if(!row.LOCAZIONE){ 
					row.hiddenButton = true;
				}
				if(!row.ID_ATTIVITA){
					row.hiddenViewActivity = true;
					row.hiddenButton = true;
				}
			});
			
			$scope.mostraLoaderGrid = false;
			
			/* reload statistiche */
			$scope.$parent.getUmStatisticheMenu(sListaMag);
			$scope.$parent.getEmStatisticheMenu(sListaMag);
				
			/* ricarico le statistiche */
			$timeout(function(){
					$scope.statisticheUI	= $scope.$parent.umStatisticheMenuCfg[$scope.stateParamsUI.section];
					console.log($scope.statisticheUI);
			},2000);
			
		},
		function(err) {
			$scope.mostraLoaderGrid = false;
			$scope.alert.error(err.data);
		});
		
	
		
	};
	
	$scope.getGrid();
	
	
		// funzione di export tabella
$scope.exportToExcel = function(){
	
	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	
	$scope.mostraLoaderUiModal = true;
	
	var confHeader	= {};
	var dataValue		=	{};
	var nomeFile 		= '';
	var nomeSheet		= '';
	
	
		
	nomeSheet = 'ATTIVITA';
	
	nomeFile	= $scope.configGestSped[$scope.stateParamsUI.section].name;
	
	angular.forEach($scope.configGridUI, function(value,key) {
		console.log(key);
		console.log(value);
		if ((key !=='FUNCTION') && value.visible === true){
			confHeader[key] = {name : value.name, id : value.name, label : value.label, visible : true, enabled : false, type:'string' };
		}
	
	});
	
	dataValue = $scope.data;
	
	// call services macrotasks


		var data = [];
		// faccio logica per definire il set dati da printare
		var dateNow = new Date();
		dateNow = $filter('date')(dateNow,'yyyyMMddHHmmss');
	
		var obj = {
				header	:	confHeader,
				data		:	dataValue
		};
		// ciclo su di esso
		obj = filterNumbers(filterDate(obj));
		console.log(obj);
		_.each(obj.data, function(row) {
			// costruzione nuovo oggetto per la stampa
			var printObj = {};
			_.each(row,function(val,key){
				// restituisce la row modificata con le sole chiavi con visibilita a true
				//console.log(confHeader[key]);
				
				if((confHeader[key]) && (confHeader[key].visible)){ printObj[confHeader[key].label] = val; }
			});
			// add all'array 
			data.push(printObj);
		});

		var ws =  XLSX.utils.json_to_sheet (data);
		// costruttore obj excel
		var wb =  XLSX.utils.book_new ();
		// appendo gli elementi al file
		XLSX.utils.book_append_sheet (wb, ws, nomeSheet );
		// scrivo il file
		XLSX.writeFile(wb, nomeFile+'_'+dateNow+'.xlsx');

		$scope.mostraLoaderUiModal = false;
		
		// jscs:enable requireCamelCaseOrUpperCaseIdentifiers

};	
	
	
	/* cambia pagina */ 
	$scope.pageChanged = function() {
		var startPos = ($scope.currentPage - 1) * $scope.itemsPerPage;
		return startPos;
	};
	
	/*$scope.viewActivity = function(item){
		// restituisce l'URL dinamico creato da
		var urlMappa =  ActivityViewServices.getUrl({
			'stile':'custom_rma_sirti',
			'id_attivita_passata':item.ID_ATTIVITA
		});
		$window.open(urlMappa,'_blank','location=no');
	};*/
	
	$scope.viewActivity = function(item){
		// restituisce l'URL dinamico creato da
		var urlMappa =  function(){
			var urlPolaris = ActivityViewServices.getUrl({
				'stile':'custom_rma_sirti',//FIXME dedicare un classe lato polaris
				'id_attivita_passata':item.ID_ATTIVITA
			});
		
			return urlPolaris;
		};
		
		//$window.open(urlMappa,'_blank','location=no');
		var urlActivity = {src: urlMappa(), title:'ACTIVITY ID : '+item.ID_ATTIVITA+' '};
	
		/* UI MODALE - Inizio */
		var $ctrl = this;
		
		$ctrl.animationsEnabled = true;
		$ctrl.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab,urlActivity) {
			/*var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;*/
			var modalInstance = $uibModal.open({
				animation: $ctrl.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/common/activityViewModal.html',
				controller: 'ActivityViewModalCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						itemSelected: function () { return itemSelected; },
						uiChiamante: function () { return uiChiamante; },
						uiConfig: function () { return uiConfig; },
						tab: function () { return tab; },
						givenUrl:function(){return urlActivity;}
				}
			});
			modalInstance.result.then(function (selectedItem) {
				$ctrl.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
				$scope.getGrid();
				
			/* reload statistiche */
			$scope.$parent.getUmStatisticheMenu(sListaMag);
			$scope.$parent.getEmStatisticheMenu(sListaMag);
				
			/* ricarico le statistiche */
			$timeout(function(){
					$scope.statisticheUI	= $scope.$parent.umStatisticheMenuCfg[$scope.stateParamsUI.section];
					console.log($scope.statisticheUI);
			},2000);
				
			});
		};
		$ctrl.open('fullscreen',null,item, $scope.configGestSped[$scope.stateParamsUI.section] ,$scope.configModalUI,'',urlActivity);
	};
	
	
	$scope.openModalUI = function (itemSelected){
		
		/* UI MODALE - Inizio */
		
		var $ctrl = this;
		
		$ctrl.animationsEnabled = true;
		
		$ctrl.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab) {
			/*var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;*/
			var modalInstance = $uibModal.open({
				animation: $ctrl.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/gestioneSpedizioni/modalGestioneSpedizioneUI.html',
				controller: 'modalGestioneSpedizioneCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						itemSelected: function () {
							return itemSelected;
						},
						uiChiamante: function () {
							return uiChiamante;
						},
						uiConfig: function () {
							return uiConfig;
						},
						tab: function () {
							return tab;
						}
				}
			});
		
			modalInstance.result.then(function (selectedItem) {
				$ctrl.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
				$scope.getGrid();
			/* reload statistiche */
			$scope.$parent.getUmStatisticheMenu(sListaMag);
			$scope.$parent.getEmStatisticheMenu(sListaMag);
				
			/* ricarico le statistiche */
			$timeout(function(){
					$scope.statisticheUI	= $scope.$parent.umStatisticheMenuCfg[$scope.stateParamsUI.section];
					console.log($scope.statisticheUI);
			},2000);
			});
		};
		
		
		$ctrl.open('fullscreen',null,itemSelected,$scope.configGestSped[$scope.stateParamsUI.section],$scope.configModalUI,'');
	};
	
	/* UI MODALE - Fine */	

}


angular.module('guiMtzNocMasterV2').controller('gsRmaRiparazioneCtrl', gsRmaRiparazioneCtrl);