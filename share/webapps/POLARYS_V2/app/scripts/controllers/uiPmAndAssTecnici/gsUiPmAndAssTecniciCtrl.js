'use strict';



function gsUiPmAndAssTecniciCtrl($rootScope,$q,$scope,$stateParams,$interval,printTableMock,$location,
	$window,$uibModal, $log, $document,getListaAttivitaUiPmAndAssTecnici,$filter,_,gettextCatalog,
	$timeout, filterNumbers, filterDate,ActivityViewServices,getGruppiInterventoPolarys,
	getTaOrdSpedCheckGrid,getTaStaffCheckGrid,getTaDicUsoCheckGrid,getTaTrasferimentoCheck,getEditFilterSnVsIdPolarys){
	
	/* global XLSX */

	$scope.stateParamsUI		= $stateParams;
	$scope.coloreUI 			= $scope.configMainMenu[$scope.stateParamsUI.section].colorB;
	$scope.coloreColUI		= $scope.configMainMenu[$scope.stateParamsUI.section].color;
	$scope.labelUI				= $scope.configMainMenu[$scope.stateParamsUI.section].label;
	$scope.iconUI					= $scope.configMainMenu[$scope.stateParamsUI.section].icon;
	$scope.icon2UI				= $scope.configMainMenu[$scope.stateParamsUI.section].icon2;
	$scope.subLabelUI			= $scope.configMainMenu[$scope.stateParamsUI.section].subLabel;
	$scope.itemGlobalSelected = {};
	
	$scope.buttonFunctionGrid = {
		info			  : {icon:'fa fa-info-circle'		,label:'Info Attivita\'', visible: false, classe : 'btn btn-default btn-xs'},
		edita 		  : {icon:'fa fa-pencil'				,label:'Edita'			, visible: false, ngclick : '$ctrl.open()', classe : 'btn btn-primary btn-xs'},
		conferma 	  : {icon:'fa fa-check'					,label:'Conferma'		, visible: false, classe : 'btn btn-primary btn-xs'},
		vedi 			  : {icon:'fa fa-external-link'	,label:'Vedi Attivita'	, visible: false, classe : 'btn btn-info btn-xs'},
		preview 	  : {icon:'fa fa fa-eye'				,label:'Preview File Ddt'	, visible: false, classe : 'btn btn-info btn-xs'},
		download 	  : {icon:'fa fa fa-download'		,label:'Salva'			, visible: false, classe : 'btn btn-default btn-xs'},
		checkAction	: {icon:'fa fa-pencil'				,label:'Verifica Azioni'			, visible: true, ngclick : '', classe : 'btn btn-primary btn-xs'},
	};
	

	$scope.aAzioniDisponibiliUi = [
		{ value	:	'PRELEVATO',Label	:	'Prelevato'},
		{ value	:	'MATERIALE_RICEVUTO',Label	:	'Materiale Ricevuto'},
		{ value	:	'DICHIARAZIONE_USO_MATERIALE',Label	:	'Dichiarazione Uso Materiale'},
	];
	

	//################### PROFILO AZIONI #########################
	$scope.profiloUI 		= $scope.$parent.profilo;
	
	$scope.showFunction = true;
	
	$scope.filterGruppiIntervento = {};
	$scope.pCallGrid = {};
	
	if ($scope.profiloUI.admin){
		$scope.filterGruppiIntervento = {filter_NOME_equals : 'ROOT'};
		//$scope.filterGruppiIntervento = {filter_NOME_in : 'PROJECT MANAGER POLARYS,ASSISTENTE TECNICO POLARYS'};
	} else if ($scope.profiloUI.AT && !$scope.profiloUI.PM) {
		$scope.filterGruppiIntervento = {filter_NOME_equals : 'ASSISTENTE TECNICO POLARYS'};
	} else if ($scope.profiloUI.PM && !$scope.profiloUI.AT) {
		$scope.filterGruppiIntervento = {filter_NOME_equals : 'PROJECT MANAGER POLARYS'};
	} else if ($scope.profiloUI.PM && $scope.profiloUI.AT) {
		$scope.filterGruppiIntervento = {filter_NOME_in : 'PROJECT MANAGER POLARYS,ASSISTENTE TECNICO POLARYS'};
	}
	
	
	/* add 26/11/2020 - Nuova gestione della sezione Filtri  - Inizio */
	
	/* model dei filtri */
	$scope.modelGuiPmAndAssTecnici = {
			SERIAL_NUMBER	:	''
		};
	
	/* disabilita filter edit */
	$scope.disabledEditGuiPmAndAssTecnici = {
			SERIAL_NUMBER	:	false
		};
		
	/* no resultfilter edit */
	$scope.noResultsEditGuiPmAndAssTecnici = {
			SERIAL_NUMBER	:	undefined
		};
		
		
	//procedura che gestisce i filtri in autocomplete 
	$scope.getCercaMateriale = function(sValue,sChiamante,sFilter){
        console.log('getCercaMateriale',sValue,sChiamante,sFilter);
		$scope.mostraLoaderAutocomplete = true;		
		
		/* rispristino tutto */
		
		/* se il filtro e' definito lo resetto nella grid */
		if (($scope.options.global) && ($scope.options.global.ID_ATTIVITA)) {
			$scope.options.global = {};
			$scope.options.global.ID_ATTIVITA = '';
		} else {
			$scope.options.global = {};
		}
		
		$scope.modelGuiPmAndAssTecnici[sChiamante]			= undefined;
		$scope.disabledEditGuiPmAndAssTecnici[sChiamante]	= false;
		$scope.noResultsEditGuiPmAndAssTecnici[sChiamante]	= undefined;
		
		
		var filtroMat = {};
		
		
		filtroMat['filter_'+sChiamante+'_'+sFilter] = sValue;
		
		if (sChiamante === 'SERIAL_NUMBER') {

			return getEditFilterSnVsIdPolarys.getData(filtroMat).$promise.then(function(result) {
					console.log(result);
					if(!result.data.results){
						//$scope.alert.error('Codice Sirti inesistente', { ttl: 5000 });
						$scope.mostraLoaderAutocomplete = false;
						return;
					}
					
					var uniqArr = _.uniq(result.data.results, function(obj) { return obj.SERIAL_NUMBER; });
					
					$scope.mostraLoaderAutocomplete = false;
					
					return _.sortBy(uniqArr,'SERIAL_NUMBER');
				
			},function(err) {
				console.log(err);
				$scope.mostraLoaderAutocomplete = false;
				$scope.alert.error(err, { ttl: 10000 });
				return;
			});
		} else {
			$scope.alert.error('Filtro non configurato', { ttl: 10000 });
		}

	};
		
	/* procedura che gestisce la selezione del materiale nel filtro */
	
    $scope.materialeSelected = function ($item, $model,sChiamante) {
        console.log('materialeSelected',$model,sChiamante);
		
		/* se il filtro e' definito lo resetto nella grid */
		if (($scope.options.global) && ($scope.options.global.ID_ATTIVITA)) {
			$scope.options.global = {};
			$scope.options.global.ID_ATTIVITA = '';
		} else {
			$scope.options.global = {};
		}
		
		/* assegno il filtro alla grid */
		$scope.options.global.ID_ATTIVITA = $scope.modelGuiPmAndAssTecnici[sChiamante].ID_POLARYS;
		
		
	};
	
	/* procedura monitora i filtri */
	$scope.monitorFiltri = function (sChiamante) {
		console.log('monitorFiltri',sChiamante);
		if (!$scope.modelGuiPmAndAssTecnici[sChiamante]) {
			/* se il filtro e' definito lo resetto nella grid */
			if (($scope.options.global) && ($scope.options.global.ID_ATTIVITA)) {
				$scope.options.global = {};
				$scope.options.global.ID_ATTIVITA = '';
			} else {
				$scope.options.global = {};
			}
		}
	};
	
	/* add 26/11/2020 - Nuova gestione della sezione Filtri  - Fine */
	
	
	
	/*gestione dei contratti e tipi intervento in base al gruppo operatore*/
	$timeout(function(){
	
		getGruppiInterventoPolarys.getData($scope.filterGruppiIntervento).$promise.then(function(result) {
				
				console.log(result.data.results);
				
				/*result.data.results = _.uniq(result.data.results,'TIPO_INTERVENTO');
				result.data.results = _.uniq(result.data.results,'TIPO_INTERVENTO');*/
				
				$scope.pCallGrid = {};
				
				//$scope.pCallGrid.filter_STATO_equals = 'FINE PIANIFICAZIONE';
				if (!$scope.profiloUI.admin){
					if ( angular.isUndefined($scope.pCallGrid.filter_CONTRATTO_in) ) {
							$scope.pCallGrid.filter_CONTRATTO_in = _.pluck(_.uniq(result.data.results,'CONTRATTO'),'CONTRATTO').toString();
					} 
					if ( angular.isUndefined($scope.pCallGrid.filter_TIPO_INTERVENTO_in) ) {
							$scope.pCallGrid.filter_TIPO_INTERVENTO_in = _.pluck(_.uniq(result.data.results,'TIPO_INTERVENTO'),'TIPO_INTERVENTO').toString();
					}
				}
			
				$scope.getGrid();
		});
	

	},0);			

	
	
	
	
	/* gestione mostra nascondi colonne grid */
	$scope.selectAllColonneGrid = function(option) {
		_.each($scope.configGridUI, function(value,key) {
			
			if (value.enabled && key !== 'FUNCTION'){
				
				value.visible = option==='TUTTI' ? true:false;
			}
		});
	};		

	$scope.configGridUI		= {
		FUNCTION: {
		    divider: false,
		    name: 'FUNCTION',
		    label: '',
		    visible: false,
		    orderable: false,
		    filtered: false,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '250px',
		    icon: 'fa-cogs',
		    enabled: false,
				enableFilter : false,
		    type:'string'
		  },
		  SOCIETA: {
		    divider: false,
		    name: 'SOCIETA',
		    label: 'SOCIETA',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
			enableFilter : true,
		    type:'string'
		  },
		  ID_ATTIVITA: {
		    divider: false,
		    name: 'ID_ATTIVITA',
		    label: 'ID POLARYS',
		    visible: true,
		    orderable: true,
		    filtered: false,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '130px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'number'
		  },
		  STATO: {
		    divider: false,
		    name: 'STATO',
		    label: 'STATO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },
		  DATA_APERTURA: {
		    divider: false,
		    name: 'DATA_APERTURA',
		    label: 'DATA APERTURA',
		    visible: true,
		    orderable: true,
		    filtered: false,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '120px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'date'
		  },
		  CONTRATTO: {
		    divider: true,
		    name: 'CONTRATTO',
		    label: 'CONTRATTO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  TIPO_INTERVENTO: {
		    divider: true,
		    name: 'TIPO_INTERVENTO',
		    label: 'TIPO INTERVENTO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
			/* colonne operative della UI - Inizio */
		  ORDINI_SPEDIZIONE_FULL: {
		    divider: true,
		    name: 'ORDINI_SPEDIZIONE_FULL',
		    label: 'ORD. SPED.',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: 'bg-aqua-active color-palette', /* $scope.configMainMenu[$scope.stateParamsUI.section].color */
		    width: '150px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'action'
		  },
		  ORD_SPED_AZ_PRELEVATO: {
		    divider: true,
		    name: 'ORD_SPED_AZ_PRELEVATO',
		    label: 'ORD. SPED. - PRELEVATO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  ORD_SPED_AZ_MAT_RICEVUTO: {
		    divider: true,
		    name: 'ORD_SPED_AZ_MAT_RICEVUTO',
		    label: 'ORD. SPED. - RICEVUTO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },			
		  STAFFETTA_FULL: {
		    divider: true,
		    name: 'STAFFETTA_FULL',
		    label: 'STAFFETTA',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: 'bg-aqua-active color-palette', /* $scope.configMainMenu[$scope.stateParamsUI.section].color */
		    width: '150px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'action'
		  },
		  STAFFETTA_AZ_PRELEVATO: {
		    divider: true,
		    name: 'STAFFETTA_AZ_PRELEVATO',
		    label: 'STAFFETTA - PRELEVATO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  STAFFETTA_AZ_MAT_RICEVUTO: {
		    divider: true,
		    name: 'STAFFETTA_AZ_MAT_RICEVUTO',
		    label: 'STAFFETTA - RICEVUTO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },			
		  DIC_USO_MAT_FULL: {
		    divider: true,
		    name: 'DIC_USO_MAT_FULL',
		    label: 'DIC. USO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: 'bg-aqua-active color-palette', /* $scope.configMainMenu[$scope.stateParamsUI.section].color */
		    width: '150px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'action'
		  },
		  /* add 06/10/2020 - Richiesta 56 - nuova colonna TRASFERIMENTI */
		  TRASFERIMENTO_FULL: {
		    divider: true,
		    name: 'TRASFERIMENTO',
		    label: 'TRASFERIMENTO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: 'bg-aqua-active color-palette', /* $scope.configMainMenu[$scope.stateParamsUI.section].color */
		    width: '150px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'action'
		  },
			/* colonne operative della UI - Inizio */
		  SITO_INTERVENTO: {
		    divider: true,
		    name: 'SITO_INTERVENTO',
		    label: 'SITO INTERVENTO',
		    visible: true,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '500px',
		    icon: '',
		    enabled: false,
				enableFilter : true,
		    type:'string'
		  },
		  RUOLO_SQUADRA_INTERVENTO: {
		    divider: true,
		    name: 'RUOLO_SQUADRA_INTERVENTO',
		    label: 'RA SQUADRA INTERVENTO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '200px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },
		  MAGAZZINO_RIFERIMENTO: {
		    divider: true,
		    name: 'MAGAZZINO_RIFERIMENTO',
		    label: 'MAGAZ. RIF.',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '100px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },
		  PROGETTO: {
		    divider: true,
		    name: 'PROGETTO',
		    label: 'PROGETTO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },
		  SOTTO_PROGETTO: {
		    divider: true,
		    name: 'SOTTO_PROGETTO',
		    label: 'SOTTO_PROGETTO',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },
		  PROPRIETA: {
		    divider: true,
		    name: 'PROPRIETA',
		    label: 'PROPRIETA',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },
		  CLIENTE_FINALE: {
		    divider: true,
		    name: 'CLIENTE_FINALE',
		    label: 'CLIENTE FINALE',
		    visible: false,
		    orderable: true,
		    filtered: true,
		    color: $scope.configMainMenu[$scope.stateParamsUI.section].color,
		    width: '150px',
		    icon: '',
		    enabled: true,
				enableFilter : true,
		    type:'string'
		  },

	};


	/* configurazione della modale invocata dalla UI */
	$scope.configModalUI =	{
		SELECTED: {
		    name: 'SELECTED',
		    label: '',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    icon:'fa fa-cogs',
		    action: '',
		    classe:{'width': '50px', 'min-width': '50px', 'max-width': '50px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  /*
		  SOCIETA: {
		    name: 'SOCIETA',
		    label: 'SOCIETA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
			classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
			visibleCol: true,
			mostraNascondi: false
		  },
		  */
		  ID_ATTIVITA: {
		    name: 'ID_ATTIVITA',
		    label: 'ID ATTIVITA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  ID_ATTIVITA_MASTER: {
		    name: 'ID_ATTIVITA_MASTER',
		    label: 'ID POLARYS',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  STATO: {
		    name: 'STATO',
		    label: 'STATO',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  DATA_APERTURA: {
		    name: 'DATA_APERTURA',
		    label: 'DATA APERTURA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  ID_SIRTI: {
		    name: 'ID_SIRTI',
		    label: 'ID SIRTI',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  PART_NUMBER: {
		    name: 'PART_NUMBER',
		    label: 'PART NUMBER',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  SERIAL_NUMBER: {
		    name: 'SERIAL_NUMBER',
		    label: 'SERIAL NUMBER',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '180px', 'min-width': '180px', 'max-width': '180px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  QUANTITA: {
		    name: 'QUANTITA',
		    label: 'QTA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '80px', 'min-width': '80px', 'max-width': '80px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  CLASSIFICAZIONE_MATERIALE: {
		    name: 'CLASSIFICAZIONE_MATERIALE',
		    label: 'CLASSIFICAZIONE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '130px', 'min-width': '130px', 'max-width': '130px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: true,
				mostraNascondi: false
		  },
		  ID_PRENOTAZIONE: {
		    name: 'ID_PRENOTAZIONE',
		    label: 'ID PRENOTAZIONE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  DICHIARAZIONE_USO: {
		    name: 'DICHIARAZIONE_USO',
		    label: 'DICH. USO',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: false
		  },			
		  MODALITA_SPEDIZIONE: {
		    name: 'MODALITA_SPEDIZIONE',
		    label: 'MOD. SPED.',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  STATO_MASTER: {
		    name: 'STATO_MASTER',
		    label: 'STATO MASTER',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  SQUADRA_MOBILE_PARTENZA: {
		    name: 'SQUADRA_MOBILE_PARTENZA',
		    label: 'SQUADRA MOBILE PARTENZA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '180px', 'min-width': '180px', 'max-width': '180px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  RA_SQUADRA_SPEDIZIONE: {
		    name: 'RA_SQUADRA_SPEDIZIONE',
		    label: 'RA SQUADRA SPEDIZIONE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '180px', 'min-width': '180px', 'max-width': '180px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },			
		  RA_SQUADRA_RICEVENTE: {
		    name: 'RA_SQUADRA_RICEVENTE',
		    label: 'RA SQUADRA RICEVENTE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '180px', 'min-width': '180px', 'max-width': '180px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },			

		  MAGAZZINO_PARTENZA: {
		    name: 'MAGAZZINO_PARTENZA',
		    label: 'MAG. PARTENZA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  MAGAZZINO_DESTINAZIONE: {
		    name: 'MAGAZZINO_DESTINAZIONE',
		    label: 'MAG. DEST.',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  PROGETTO: {
		    name: 'PROGETTO',
		    label: 'PROGETTO',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  SOTTO_PROGETTO: {
		    name: 'SOTTO_PROGETTO',
		    label: 'SOTTO_PROGETTO',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  PROPRIETA: {
		    name: 'PROPRIETA',
		    label: 'PROPRIETA',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  CLIENTE_FINALE: {
		    name: 'CLIENTE_FINALE',
		    label: 'CLIENTE_FINALE',
		    visibleInSummary: true,
		    visibleInVerify: false,
		    visibleInAction: false,
		    action: '',
				classe:{'width': '150px', 'min-width': '150px', 'max-width': '150px', 'white-space': 'normal','text-align': 'center'},
				visibleCol: false,
				mostraNascondi: true
		  },
		  MATERIALE_DA_SPEDIRE: {
		    name: 'MATERIALE_DA_SPEDIRE',
		    label: 'Materiale Da Spedire',
		    visibleInSummary: false,
		    visibleInVerify: false,
		    visibleInAction: true,
		    action: 'MATERIALE_DA_SPEDIRE',
		    isMassiva: true,
		    statoAZ: 'APPROVATO'
		  },
		  FLAG_MOD_SPED_AZ_MAT_DA_SPED: {
		    name: 'FLAG_MOD_SPED_AZ_MAT_DA_SPED',
		    label: 'MODALITA\' SPEDIZIONE',
		    visibleInSummary: false,
		    visibleInVerify: true,
		    visibleInAction: false,
		    action: 'MATERIALE_DA_SPEDIRE',
		    typeEdit: 'SELECT',
		    notNull: true
		  }
	};

	$scope.options = {
			orderBy : ['ID_ATTIVITA'],
			global : '',
			searchSingleColumn : true
	};
	$scope.currentPage  = 1;
	$scope.itemsPerPage = 10;
	$scope.orderByField = $scope.configGridUI[$scope.options.orderBy[0]].name;
	$scope.reverseSort  = false;
	$scope.maxSizePager = 6;
	$scope.nuovoNumero = 10;
	$scope.mostraLoaderGrid = true;
	
	// composto data header e data
	$scope.objectDati = {
			header:$scope.configGridUI,
			data:[]
	};
	
	
	//##################################### ATTIVAZIONE FILTRI ###################################//
	$scope.filterText = {}; // mappa chiave/valore con campo/valore da filtrare
	$scope.filterOperator = {}; // mappa chiave/valore con campo/operatore di filtro da applicare
	$scope.sortDirection = {}; // mappa chiave/valore con campo/direzione di ordinamento
	$scope.sortOrderArray = []; // array con l'elenco ordinato dei campi da ordinare
	$scope.sortOrderMap = {}; // mappa chiave/valore con i campi/posizione da ordinare. parallelo a $scope.sortOrderArray ma serve per essere passato
								// alla direttiva sirti-column-sort-and-filter nell'attributo sort-position in modo che non debba essere calcolato
	
	$scope.filterOrOrderApplied = false;
	
	function isFilterOrOrderApplied() {
		var isFiltered = false;
		var isSorted = false;
		_.each($scope.filterOperator, function(value) {
			if(!_.isUndefined(value)) {
				isFiltered = true;
			}
		});
		_.each($scope.sortOrderMap, function(value) {
			if(!_.isUndefined(value)) {
				isSorted = true;
			}
		});
		return isFiltered || isSorted;
	}
	
	$scope.$watch('filterOperator', function() {
		$scope.filterOrOrderApplied = isFilterOrOrderApplied();
	});
	
	$scope.$watch('sortOrderMap', function() {
		$scope.filterOrOrderApplied = isFilterOrOrderApplied();
	});
	
	// funzione che recepisce l'applica della direttiva sirti-column-sort-and-filter
	$scope.applyFilterAndSort = function(field, filterText, filterOperator, sortDirection) {
		if(field === 'id' && filterText && !filterText.match(/^\d+$/)) {
			$scope.alert.warning(gettextCatalog.getString('Activity Id must be a number'));
			return;
		}
	
		$scope.filterText[field] = filterText;
		$scope.filterOperator[field] = filterOperator;
		$scope.sortDirection[field] = sortDirection;
		if($scope.sortDirection[field]) {
			// la direzione di sort del campo è valorizzata e la aggiungo all'array se non ancora presente
			if(!_.contains($scope.sortOrderArray, field)) {
				$scope.sortOrderArray.push(field);
			}
		} else {
			// ...altrimenti elimino il campo dall'ordinamento
			$scope.sortOrderArray = _.without($scope.sortOrderArray, field);
		}
		// costruisco la mappa a partire dall'array di ordinamento (vedi nota sull'oggetto $scope.sortOrderMap)
		$scope.sortOrderMap = {};
		var i = 1;
		_.each($scope.sortOrderArray, function(value) {
			$scope.sortOrderMap[value] = i++;
		});
		// invoco il reload della tabella
		$scope.getGrid();
	};
	
	
	// pulisco filtri
	$scope.clearFiltersAndOrdering = function() {
		$scope.filterText = {};
		$scope.filterOperator = {};
		$scope.sortDirection = {};
		$scope.sortOrderArray = [];
		$scope.sortOrderMap = {};
		$scope.options.global = '';
		// invoco il reload della tabella
		$scope.getGrid();
	};
	
	//###################################### FINE PREPARAZIONE RICERCA #####################################//
	
	$scope.getGrid = function() {
		//var withFilter = [];//['permitsAreaId'];
		//var withOutSorter = [];
		//var withAutocompleate = [];//['macroTaskSubCategory','macroTaskCategory'];
		//var hidden = [];//['Pfp','Pop','projectId','customerId','contractId','buildingId','macroTaskType'];
		var oldTemp = $scope.data || [];
		var temp = [];
		
		$scope.mostraLoaderGrid = true;
	

	
			// #################### DEFAULT IN BASE PROFILO AZIONI ###################
		var pCall = $scope.pCallGrid;
		// #################### DEFAULT IN BASE PROFILO AZIONI  FINE###################

		// parametri in URI di ricerca su BKEND		
		var params = {
				sort:[],
				pCall : pCall
		};
		
		//################################# POPOLAMENTO FILTRI ##################################//
		
		var localFilter = {};
		// definisco i parametri di filtro (testo e operatore)
		_.each($scope.filterOperator, function(value, key) {
			if($scope.filterOperator[key] && $scope.filterText[key]) {
				// solo in questo caso setto il parametro dello skip a 0 
				// in modo da avere sempre la ripartenza dallo 0esimo
				//params.skip = 0;
				params[key + '_' + $scope.filterOperator[key]] = $scope.filterText[key];
				// popolo i filtri locali
				localFilter[key] = $scope.filterText[key];

				// remap per far funzionare l'equal
				if($scope.filterOperator[key] === 'equal'){$scope.filterOperator[key] = 'equals';}
				// aggiungo i parametri di ricerca a pCall
				pCall['filter_'+ key + '_' + $scope.filterOperator[key]] = $scope.filterText[key];
				
				if(key === 'status') {
					// per lo status sostituisco gli spazi con _ secondo la convenzione di chiamare gli stati senza spazi e con le parole divise da _
					params[key + '_' + $scope.filterOperator[key]] = params[key + '_' + $scope.filterOperator[key]].replace(/ /g, '_');
				}
			}
		});
	
		// definisco l'ordine di sort
		_.each($scope.sortOrderArray, function(key) {
			if($scope.sortDirection[key]) {
				params.sort.push(($scope.sortDirection[key] === 'ASC' ? '' : '-') + key);
			}
		});
	
		// se non c'è alcun ordinamento ordino per id DESC
		// agisco in questo punto per evitare che la direttiva sirti-column-sort-and-filter evidenzi tale ordinamento
		if(params.sort.length === 0) {	
			params.sort = $scope.options.filterBy;
		}
		// ############################################ FINE POPOLAMENTO ########################################// 
		getListaAttivitaUiPmAndAssTecnici.getData(pCall).$promise.then(function(result) {
			temp = result.data.results;
			$scope.totalItems = result.data.count;
			
			$scope.objectDati.data = temp; 
			$scope.objectDati = filterNumbers(filterDate($scope.objectDati));

			// ###################################### FILTRO LOCALE ATTIVO ##########################//
			// FIXME filtri locali in caso attivati i filtri sui campi 
			$scope.objectDati.data = $filter('orderBy')($scope.objectDati.data = $filter('filter')((temp || oldTemp),localFilter),params.sort);
			// ###################################### FILTRO LOCALE ATTIVO ##########################//
			$scope.data = $scope.objectDati.data;
			
			$scope.data = _.each($scope.objectDati.data,function(row){
				/* ORDINE SPEDIZIONE */
				row.ORDINI_SPEDIZIONE_FULL						=	0;
				row.ORDINI_SPEDIZIONE_MINUS						=	0;
				row.ORD_SPED_AZ_PRELEVATO						=	0; /* ASSEGNATO */
				row.ORD_SPED_AZ_MAT_RICEVUTO					=	0; /* IN CONSEGNA */
				/* add 08/10/2020 - richiesta 56 */
				row.ORD_SPED_AZ_FORZA_RIENTRO_MAG_INTERVENTO	=	0; /* ASSEGNATO,APERTA,IN CONSEGNA */
				
				/* STAFFETTA */
				row.STAFFETTA_FULL								= 	0;
				row.STAFFETTA_MINUS								= 	0;
				row.STAFFETTA_AZ_PRELEVATO						= 	0; /* ASSEGNATO */
				row.STAFFETTA_AZ_MAT_RICEVUTO					= 	0; /* IN CONSEGNA */
				
				/* add 08/10/2020 - richiesta 56 */
				row.STAFFETTA_AZ_FORZA_RIENTRO_MAG_INTERVENTO 	= 	0; /* ASSEGNATO,APERTA,IN CONSEGNA */

				
				/* DICHIARAZIONE USO */				
				row.DIC_USO_MAT_FULL							=	0;
				row.DIC_USO_MAT_MINUS							=	0;
				/* add 08/10/2020 - richiesta 56 */
				row.DIC_USO_MAT_AZ_DICHIARAZIONE_USO_MATERIALE	= 	0; /* APERTA */
				row.DIC_USO_AZ_FORZA_RIENTRO_MAG_INTERVENTO		=	0; /* APERTA  -> DIC_USO_MAT_AZ_DICHIARAZIONE_USO_MATERIALE non Utilizzato */
				
				
				/* TRASFERIMENTO */
				row.TRASFERIMENTO_FULL								=	0;
				row.TRASFERIMENTO_MINUS								=	0;
				/* add 08/10/2020 - richiesta 56 */
				row.TRASFERIMENTO_AZ_FORZA_RIENTRO_MAG_INTERVENTO	= 	0; /* != CHIUSA,CONSEGNATO */	
				
				row.hiddenButton = false;
				row.hiddenViewActivity = false;				
							
				// arr azioni possibili sull'item selezionato
				row.arrAzPossibili		= [];
							
				// obj info button 
				row.objInfoButtonModal	= {	ORDINI_SPEDIZIONE_FULL : '',
											STAFFETTA_FULL: '',
											DIC_USO_MAT_FULL:'',
											TRASFERIMENTO_FULL:'' /* add 06/10/2020 - nuova gestione dei Trasferimenti */
				};
				
				row.erroreCheckActionIdPolarys = false;
				
				/* se stavo elaborando una riga, allora rifaccio il check */
				if (($scope.itemGlobalSelected) && (parseInt($scope.itemGlobalSelected.ID_ATTIVITA) === parseInt(row.ID_ATTIVITA))){
					$scope.checkActionIdPolarys(row);
				} 
				
			});
			
			$scope.mostraLoaderGrid = false;
			

				
		},
		function(err) {
			$scope.mostraLoaderGrid = false;
			$scope.alert.error(err.data);
		});
		
	
		
	};
  
  
/* add 26/02/2020 - nuova procedura che esegue il check delle azioni da eseguire */
$scope.checkActionIdPolarys = function(item){
	
	$scope.itemGlobalSelected = item;
	
	console.log('checkActionIdPolarys: ',item);
	
	item.mostraLoaderCheck= true;
	
	/* resetto tutto */
	_.each($scope.data,function(row){
				/* resetto tutto */
				
				
				/* ORDINE SPEDIZIONE */
				row.ORDINI_SPEDIZIONE_FULL						=	0;
				row.ORDINI_SPEDIZIONE_MINUS						=	0;
				row.ORD_SPED_AZ_PRELEVATO						=	0; /* ASSEGNATO */
				row.ORD_SPED_AZ_MAT_RICEVUTO					=	0; /* IN CONSEGNA */
				/* add 08/10/2020 - richiesta 56 */
				row.ORD_SPED_AZ_FORZA_RIENTRO_MAG_INTERVENTO	=	0; /* ASSEGNATO,APERTA,IN CONSEGNA */
				
				/* STAFFETTA */
				row.STAFFETTA_FULL								= 	0;
				row.STAFFETTA_MINUS								= 	0;
				row.STAFFETTA_AZ_PRELEVATO						= 	0; /* ASSEGNATO */
				row.STAFFETTA_AZ_MAT_RICEVUTO					= 	0; /* IN CONSEGNA */
				
				/* add 08/10/2020 - richiesta 56 */
				row.STAFFETTA_AZ_FORZA_RIENTRO_MAG_INTERVENTO	= 	0; /* ASSEGNATO,APERTA,IN CONSEGNA */

				
				/* DICHIARAZIONE USO */				
				row.DIC_USO_MAT_FULL							=	0;
				row.DIC_USO_MAT_MINUS							=	0;
				/* add 08/10/2020 - richiesta 56 */
				row.DIC_USO_MAT_AZ_DICHIARAZIONE_USO_MATERIALE	= 	0; /* APERTA */
				row.DIC_USO_AZ_FORZA_RIENTRO_MAG_INTERVENTO		=	0; /* APERTA  -> DIC_USO_MAT_AZ_DICHIARAZIONE_USO_MATERIALE non Utilizzato */
				
				
				/* TRASFERIMENTO */
				row.TRASFERIMENTO_FULL							=	0;
				row.TRASFERIMENTO_MINUS							=	0;
				/* add 08/10/2020 - richiesta 56 */
				row.TRASFERIMENTO_AZ_FORZA_RIENTRO_MAG_INTERVENTO					= 	0; /* != CHIUSA,CONSEGNATO */				

				
				row.hiddenButton = false;
				row.hiddenViewActivity = false;				
							
				// arr azioni possibili sull'item selezionato
				row.arrAzPossibili		= [];
							
				// obj info button 
				row.objInfoButtonModal	= {	ORDINI_SPEDIZIONE_FULL : '',
											STAFFETTA_FULL: '',
											DIC_USO_MAT_FULL:'',
											TRASFERIMENTO_FULL:'' /* add 06/10/2020 - nuova gestione dei Trasferimenti */
											};
				
				row.erroreCheckActionIdPolarys = false;
	});
	
	/* ORDINE SPEDIZIONE */
	getTaOrdSpedCheckGrid.getData({filter_ID_ATTIVITA_MASTER_equals	:	item.ID_ATTIVITA}).$promise.then(function(resultTaOrdSpedCheck) {
		//console.log('getTaOrdSpedCheckGrid',resultTaOrdSpedCheck);
		
		/* STAFFETTA */
		getTaStaffCheckGrid.getData({filter_ID_ATTIVITA_MASTER_equals	:	item.ID_ATTIVITA}).$promise.then(function(resultTaStaffCheck) {
			//console.log('getTaStaffCheckGrid',resultTaStaffCheck);
			
			/* DICHIARAZIONE USO */
			getTaDicUsoCheckGrid.getData({filter_ID_ATTIVITA_MASTER_equals	:	item.ID_ATTIVITA}).$promise.then(function(resultTaDicUsoCheck) {
				//console.log('getTaDicUsoCheckGrid',resultTaDicUsoCheck);
				
				
				/* TRASFERIMENTO */

				getTaTrasferimentoCheck.getData({filter_ID_ATTIVITA_MASTER_equals	:	item.ID_ATTIVITA}).$promise.then(function(resultTaTrasferimentoCheck) {
					//console.log('getTaTrasferimentoCheck',resultTaTrasferimentoCheck);
		
					/* gestisco tutti i ritorni e dei BOTTONI OPERATIVI che devono apparire nella colonna FUNCTION */
					
					
					/* ORDINE SPEDIZIONE - Inizio */
					if (parseInt(resultTaOrdSpedCheck.data.count) > 0) {
						
						console.log('TOT resultTaOrdSpedCheck',resultTaOrdSpedCheck.data.count);
						console.log('getTaOrdSpedCheckGrid',resultTaOrdSpedCheck.data.results);
						
						
						item.objInfoButtonModal.ORDINI_SPEDIZIONE_FULL = {
																			context				: 'ORD. SPED',
																			tabella				: 'TA_ORDINE_SPEDIZIONE',
																			filterTabella		: {STATO : 'ASSEGNATO,IN CONSEGNA,APERTA'}, /* modify 06/10/2020 - richiesta 56 - FORZA_RIENTRO */
																			minus				:	0/* row.ORDINI_SPEDIZIONE_MINUS */
																		};
						
						
						item.ORDINI_SPEDIZIONE_MINUS		=	0;
	
	
	
						// controllo se ci sono piu azioni
						
						/* modify 06/10/2020 - richiesta 56 - PRELEVATO  - ASSEGNATO */
						var arrAzPrelevatoTaOrdSped = _.where(resultTaOrdSpedCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_PRELEVATO' , STATO_ATTIVITA : 'ASSEGNATO'} );
						
						/* modify 06/10/2020 - richiesta 56 - MATERIALE_RICEVUTO - IN CONSEGNA */
						var arrAzMatRicTaOrdSped = _.where(resultTaOrdSpedCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_MATERIALE_RICEVUTO' , STATO_ATTIVITA : 'IN CONSEGNA'} );
						
						/* add 06/10/2020 - richiesta 56 - AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED - ASSEGNATO */
						var arrAzForzaRientroMagInterventoTaOrdSpedAssegnato = _.where(resultTaOrdSpedCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED', STATO_ATTIVITA : 'ASSEGNATO'} ); 
	
						/* add 06/10/2020 - richiesta 56 - AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED  - APERTA*/
						var arrAzForzaRientroMagInterventoTaOrdSpedAperta = _.where(resultTaOrdSpedCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED', STATO_ATTIVITA : 'APERTA'} ); 		

						/* add 06/10/2020 - richiesta 56 - AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED  - IN CONSEGNA */
						var arrAzForzaRientroMagInterventoTaOrdSpedInConsegna = _.where(resultTaOrdSpedCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED', STATO_ATTIVITA : 'IN CONSEGNA'} ); 	
	
						
						item.ORDINI_SPEDIZIONE_FULL			=	arrAzPrelevatoTaOrdSped.length + arrAzMatRicTaOrdSped.length + arrAzForzaRientroMagInterventoTaOrdSpedAperta.length;
						
						
						
						/* modify 06/10/2020 - richiesta 56 - PRELEVATO  - ASSEGNATO */
						console.log('arrAzPrelevatoTaOrdSped',arrAzPrelevatoTaOrdSped);
						
						/* modify 06/10/2020 - richiesta 56 - MATERIALE_RICEVUTO - IN CONSEGNA */
						console.log('arrAzMatRicTaOrdSped',arrAzMatRicTaOrdSped);
						
						/* add 06/10/2020 - richiesta 56 - AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED - ASSEGNATO */
						console.log('arrAzForzaRientroMagInterventoTaOrdSpedAssegnato',arrAzForzaRientroMagInterventoTaOrdSpedAssegnato);
						
						/* add 06/10/2020 - richiesta 56 - AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED  - APERTA*/
						console.log('arrAzForzaRientroMagInterventoTaOrdSpedAperta',arrAzForzaRientroMagInterventoTaOrdSpedAperta);
						
						/* add 06/10/2020 - richiesta 56 - AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED  - IN CONSEGNA */
						console.log('arrAzForzaRientroMagInterventoTaOrdSpedInConsegna',arrAzForzaRientroMagInterventoTaOrdSpedInConsegna);
						
						
						/* creo un unico array per l'azione AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED */
						
						var arrAzForzaRientroMagInterventoTaOrdSpedFull = _.union(arrAzForzaRientroMagInterventoTaOrdSpedAssegnato,arrAzForzaRientroMagInterventoTaOrdSpedAperta,arrAzForzaRientroMagInterventoTaOrdSpedInConsegna);
						
						console.log('arrAzForzaRientroMagInterventoTaOrdSpedFull',arrAzForzaRientroMagInterventoTaOrdSpedFull);						
						
						
						
						
						// controllo se gli array delle action distinte sono pieni e li gestisco
						
						/* PRELEVATO  - ASSEGNATO - Inizio */
						if (arrAzPrelevatoTaOrdSped.length > 0) {
							
							/* aggiungo l'azione all arrAzPossibili */
							item.ORD_SPED_AZ_PRELEVATO			=	arrAzPrelevatoTaOrdSped.length;
							
							console.log('ORDINI_SPEDIZIONE -> AZ_PRELEVATO');
							
							item.arrAzPossibili.push({ context: 'ORD. SPED',
																				value	:	'PRELEVATO',
																				label	:	'Prelevato',
																				nMat:arrAzPrelevatoTaOrdSped.length,
																				padre:'ORDINI_SPEDIZIONE_FULL',
																				tabella:'TA_ORDINE_SPEDIZIONE',
																				filterTabella: {STATO : 'ASSEGNATO' },
																				plsql: 'Prelevato'
																				});
						} /* PRELEVATO  - ASSEGNATO  - Fine*/
						
						/* MATERIALE_RICEVUTO - IN CONSEGNA - Inizio */
						if (arrAzMatRicTaOrdSped.length > 0) {
							
							/* aggiungo l'azione all arrAzPossibili */
							
							console.log('ORDINI_SPEDIZIONE -> AZ_MAT_RICEVUTO');
							
							item.ORD_SPED_AZ_MAT_RICEVUTO		=	arrAzMatRicTaOrdSped.length;
							
	
							item.arrAzPossibili.push({ context: 'ORD. SPED',
																				value	:	'MATERIALE_RICEVUTO',
																				label	:	'Materiale Ricevuto',
																				nMat:arrAzMatRicTaOrdSped.length,
																				padre:'ORDINI_SPEDIZIONE_FULL',
																				tabella:'TA_ORDINE_SPEDIZIONE',
																				filterTabella: {STATO : 'IN CONSEGNA' },
																				plsql: 'MaterialeRicevuto'
																				});
						} /* MATERIALE_RICEVUTO - IN CONSEGNA - Fine */
						
			
							/* add 06/10/2020 - richiesta 56 - FORZA_RIENTRO - FULL - Inizio */
						if (arrAzForzaRientroMagInterventoTaOrdSpedFull.length > 0) {
							
							/* aggiungo l'azione all arrAzPossibili */
							
							console.log('ORDINI_SPEDIZIONE -> AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED');
							
							item.ORD_SPED_AZ_FORZA_RIENTRO_MAG_INTERVENTO	=	arrAzForzaRientroMagInterventoTaOrdSpedFull.length;
							
	
							item.arrAzPossibili.push({	context: 'ORD. SPED',
														value	:	'FORZA_RIENTRO_MAG_INTERVENTO',
														label	:	'Forza Rientro Magazzino Intervento',
														nMat:arrAzForzaRientroMagInterventoTaOrdSpedFull.length,
														padre:'ORDINI_SPEDIZIONE_FULL',
														tabella:'TA_ORDINE_SPEDIZIONE',
														filterTabella: {STATO : 'ASSEGNATO,IN CONSEGNA,APERTA' },
														plsql: 'ForzaRientroMagIntervOrdSped'
														});
						} /* FORZA_RIENTRO - FULL - fine */					
						
						
					} /* ORDINE SPEDIZIONE - Fine */
					
					/* STAFFETTA - Inizio */
					if (parseInt(resultTaStaffCheck.data.count) > 0) {
						
						console.log('TOT resultTaStaffCheck',resultTaStaffCheck.data.count);
						console.log('getTaStaffCheckGrid',resultTaStaffCheck.data.results);
						
						item.objInfoButtonModal.STAFFETTA_FULL = {
																						context				: 'STAFFETTA',
																						tabella				:	'TA_STAFFETTA',
																						filterTabella	: {STATO : 'ASSEGNATO,IN CONSEGNA,APERTA' }, /*{STATO : 'ASSEGNATO,IN CONSEGNA,APERTA' },*/
																						minus					:	0 /* row.STAFFETTA_MINUS */
																					};
																					
																					
						item.STAFFETTA_FULL							= parseInt(resultTaStaffCheck.data.count);
						item.STAFFETTA_MINUS						= 0;
	
						
						// controllo se ci sono piu azioni
						
						/* modify 06/10/2020 - richiesta 56 - PRELEVATO  - ASSEGNATO */
						var arrAzPrelevatoTaStaff = _.where(resultTaStaffCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_PRELEVATO'  , STATO_ATTIVITA : 'ASSEGNATO' } );
						
						/* modify 06/10/2020 - richiesta 56 - MATERIALE_RICEVUTO - IN CONSEGNA */
						var arrAzMatRicTaStaff = _.where(resultTaStaffCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_MATERIALE_RICEVUTO' , STATO_ATTIVITA : 'IN CONSEGNA'} );
						
						/* add 06/10/2020 - richiesta 56 - FORZA_RIENTRO - ASSEGNATO */
						var arrAzForzaRientroMagInterventoTaStaffAssegnato = _.where(resultTaStaffCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_FORZA_RIENTRO_MAG_INTERVENTO_STAFF', STATO_ATTIVITA : 'ASSEGNATO'} ); 
	
						/* add 06/10/2020 - richiesta 56 - FORZA_RIENTRO  - APERTA*/
						var arrAzForzaRientroMagInterventoTaStaffAperta = _.where(resultTaStaffCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_FORZA_RIENTRO_MAG_INTERVENTO_STAFF', STATO_ATTIVITA : 'APERTA'} ); 		

						/* add 06/10/2020 - richiesta 56 - ANNULLATA  - IN CONSEGNA */
						var arrAzForzaRientroMagInterventoTaStaffInConsegna = _.where(resultTaStaffCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_FORZA_RIENTRO_MAG_INTERVENTO_STAFF', STATO_ATTIVITA : 'IN CONSEGNA'} ); 	


						/* fixme - forzo lo svuotamento x evitare che si vedano le azioni - da usare in futuro se richiesto - Inizio */
						
						//arrAzForzaRientroMagInterventoTaStaffAssegnato	= [];
						//arrAzForzaRientroMagInterventoTaStaffAperta		= [];
						//arrAzForzaRientroMagInterventoTaStaffInConsegna	= [];
						
						/* fixme - forzo lo svuotamento x evitare che si vedano le azioni - da usare in futuro se richiesto - Fine */
						
						
						/* creo un unico array per l'azione AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED */
						
						var arrAzForzaRientroMagInterventoTaStaffFull = _.union(arrAzForzaRientroMagInterventoTaStaffAssegnato,arrAzForzaRientroMagInterventoTaStaffAperta,arrAzForzaRientroMagInterventoTaStaffInConsegna);
						
						console.log('arrAzForzaRientroMagInterventoTaStaffFull',arrAzForzaRientroMagInterventoTaStaffFull);	
						
						item.STAFFETTA_FULL			=	arrAzPrelevatoTaStaff.length + arrAzMatRicTaStaff.length + arrAzForzaRientroMagInterventoTaStaffAperta.length;
						
						

						
						console.log('arrAzPrelevatoTaStaff',arrAzPrelevatoTaStaff);
						console.log('arrAzMatRicTaStaff',arrAzMatRicTaStaff);
						console.log('arrAzForzaRientroMagInterventoTaStaffAssegnato',arrAzForzaRientroMagInterventoTaStaffAssegnato);
						console.log('arrAzForzaRientroMagInterventoTaStaffAperta',arrAzForzaRientroMagInterventoTaStaffAperta);
						console.log('arrAzForzaRientroMagInterventoTaStaffInConsegna',arrAzForzaRientroMagInterventoTaStaffInConsegna);
						
						/* PRELEVATO  - ASSEGNATO - Inizio */
						if (arrAzPrelevatoTaStaff.length > 0) {
							console.log('STAFFETTA -> AZ_PRELEVATO');
							
							item.arrAzPossibili.push({	context: 'STAFFETTA',
																				value	:	'PRELEVATO',
																				label	:	'Prelevato',
																				nMat:arrAzPrelevatoTaStaff.length,
																				padre:'STAFFETTA_FULL',
																				tabella:'TA_STAFFETTA',
																				filterTabella: {STATO : 'ASSEGNATO' },
																				plsql: 'Prelevato'
																			});
							
						item.STAFFETTA_AZ_MAT_RICEVUTO	= arrAzPrelevatoTaStaff.length;
						
						
						} /* PRELEVATO  - ASSEGNATO -  Fine*/
						
						/* MATERIALE_RICEVUTO - IN CONSEGNA - Inizio */
						if (arrAzMatRicTaStaff.length > 0) {
							console.log('STAFFETTA -> AZ_MAT_RICEVUTO');
							
							item.arrAzPossibili.push({	context: 'STAFFETTA',
																				value	:	'MATERIALE_RICEVUTO',
																				label	:	'Materiale Ricevuto',
																				nMat	:arrAzMatRicTaStaff.length,
																				padre	:'STAFFETTA_FULL',
																				tabella:'TA_STAFFETTA',
																				filterTabella: {STATO : 'IN CONSEGNA' },
																				plsql: 'MaterialeRicevuto'
																			});
							
							item.STAFFETTA_AZ_PRELEVATO			= arrAzMatRicTaStaff.length;
							
						} /* MATERIALE_RICEVUTO - IN CONSEGNA - Fine*/
						
						/* add 06/10/2020 - richiesta 56 - FORZA_RIENTRO - full - Inizio */
						
						if (arrAzForzaRientroMagInterventoTaStaffFull.length > 0) {
							
							/* aggiungo l'azione all arrAzPossibili */
							
							console.log('STAFFETTA -> AZ_FORZA_RIENTRO_MAG_INTERVENTO_ORD_SPED');
							
							item.STAFFETTA_AZ_FORZA_RIENTRO_MAG_INTERVENTO		=	arrAzForzaRientroMagInterventoTaStaffFull.length;
							
	
							item.arrAzPossibili.push({	context: 'STAFFETTA',
														value	:	'FORZA_RIENTRO_MAG_INTERVENTO',
														label	:	'Forza Rientro Magazzino Intervento',
														nMat:arrAzForzaRientroMagInterventoTaStaffFull.length,
														padre:'STAFFETTA_FULL',
														tabella:'TA_STAFFETTA',
														filterTabella: {STATO : 'ASSEGNATO,IN CONSEGNA,APERTA' },
														plsql: 'ForzaRientroMagIntervStaff'
														});
						} /* FORZA_RIENTRO - full - fine */						

						
						
						
						
					} /* STAFFETTA - Fine */
					
					/* DICHIARAZIONE USO - Inizio */
					if (parseInt(resultTaDicUsoCheck.data.count) > 0) {
						
						console.log('TOT resultTaDicUsoCheck',resultTaDicUsoCheck.data.count);
						console.log('getTaDicUsoCheckGrid',resultTaDicUsoCheck.data.results);
						
						
						item.objInfoButtonModal.DIC_USO_MAT_FULL = {
																					context				: 'DIC. USO',
																					tabella				:	'TA_DICHIARAZIONE_USO_MATERIALE',
																					filterTabella	: {STATO : 'APERTA' },
																					minus					:	0 /* row.DIC_USO_MAT_MINUS */
																					};
	
						item.DIC_USO_MAT_FULL						=	parseInt(resultTaDicUsoCheck.data.count);
						item.DIC_USO_MAT_MINUS					=	0;
						
						
						/* add 06/10/2020 - richiesta 56 - AZ_DICHIARAZIONE_USO_MATERIALE */
						var arrAzDicUsoMaterialeTaDicUso = _.where(resultTaDicUsoCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_DICHIARAZIONE_USO_MATERIALE', STATO_ATTIVITA : 'APERTA'} ); 
	
						/* add 06/10/2020 - richiesta 56 - AZ_FORZA_RIENTRO_MAG_INTERVENTO_DIC_USO */
						var arrAzForzaRientroMagInterventoTaDicUsoFull = _.where(resultTaDicUsoCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_FORZA_RIENTRO_MAG_INTERVENTO_DIC_USO', STATO_ATTIVITA : 'APERTA'} ); 	
						
						
						
						item.DIC_USO_MAT_FULL =	(arrAzDicUsoMaterialeTaDicUso.length > 0 ) ? arrAzDicUsoMaterialeTaDicUso.length : arrAzForzaRientroMagInterventoTaDicUsoFull.length;
						
						/* add 06/10/2020 - richiesta 56 - AZ_DICHIARAZIONE_USO_MATERIALE */
						console.log('arrAzDicUsoMaterialeTaDicUso',arrAzDicUsoMaterialeTaDicUso);
						
						/* add 06/10/2020 - richiesta 56 - AZ_ANNULLAMENTO */
						console.log('arrAzForzaRientroMagInterventoTaDicUsoFull',arrAzForzaRientroMagInterventoTaDicUsoFull);										
						
						/* add 06/10/2020 - richiesta 56 - AZ_DICHIARAZIONE_USO_MATERIALE */
						
						if (arrAzDicUsoMaterialeTaDicUso.length > 0) {
							
							/* aggiungo l'azione all arrAzPossibili */
							
							console.log('DIC_USO_MAT -> AZ_DICHIARAZIONE_USO_MATERIALE');
							
							item.DIC_USO_MAT_AZ_DICHIARAZIONE_USO_MATERIALE	=	arrAzDicUsoMaterialeTaDicUso.length;
							
	
							item.arrAzPossibili.push({ context: 'DIC. USO',
																			value	:	'DICHIARAZIONE_USO_MATERIALE',
																			label	:	'Dichiarazione Uso Materiale',
																			nMat:arrAzDicUsoMaterialeTaDicUso.length,
																			padre:'DIC_USO_MAT_FULL',
																			tabella:'TA_DICHIARAZIONE_USO_MATERIALE',
																			filterTabella: {STATO : 'APERTA' },
																			plsql: 'DichiarazioneUsoMateriale'
																		});
						}
						
						/* add 06/10/2020 - richiesta 56 - FORZA_RIENTRO - full*/
						
						if (arrAzForzaRientroMagInterventoTaDicUsoFull.length > 0) {
							
							/* aggiungo l'azione all arrAzPossibili */
							
							console.log('DIC_USO_MAT -> AZ_FORZA_RIENTRO_MAG_INTERVENTO_DIC_USO');
							
							item.DIC_USO_AZ_FORZA_RIENTRO_MAG_INTERVENTO	=	arrAzForzaRientroMagInterventoTaDicUsoFull.length;
							
	
							item.arrAzPossibili.push({ 	context	: 	'DIC. USO',
														value	:	'FORZA_RIENTRO_MAG_INTERVENTO',
														label	:	'Forza Rientro Magazzino Intervento',
														nMat	:	arrAzForzaRientroMagInterventoTaDicUsoFull.length,
														padre	:	'DIC_USO_MAT_FULL',
														tabella	:	'TA_DICHIARAZIONE_USO_MATERIALE',
														filterTabella: {STATO : 'APERTA' },
														plsql	: 	'ForzaRientroMagIntervDicUso'
													});
						}	
						
						
	
						
					} /* DICHIARAZIONE USO - Fine */
					
					/* TRASFERIMENTO - Inizio */
					if (parseInt(resultTaTrasferimentoCheck.data.count) > 0) {
						
						console.log('TOT resultTaTrasferimentoCheck',resultTaTrasferimentoCheck.data.count);
						console.log('getTaTrasferimentoCheck',resultTaTrasferimentoCheck.data.results);
						
						
						
						item.objInfoButtonModal.TRASFERIMENTO_FULL = {
																					context				:	'TRASFERIMENTO',
																					tabella				:	'TA_TRASFERIMENTO',
																					filterTabella		:	{NOT_IN_STATO : 'CHIUSA,CORRIERE' }, /* NOT IN */
																					minus				:	0 /* row.DIC_USO_MAT_MINUS */
																					};
	
						item.TRASFERIMENTO_FULL					=	parseInt(resultTaTrasferimentoCheck.data.count);
						item.TRASFERIMENTO_MINUS				=	0;						
						
						/* add 06/10/2020 - richiesta 56 - AZ_FORZA_RIENTRO_MAG_INTERVENTO_TRASF */
						var arrAzForzaRientroMagInterventoTrasfFull = _.where(resultTaTrasferimentoCheck.data.results , { AZIONE_DA_ESEGUIRE : 'AZ_FORZA_RIENTRO_MAG_INTERVENTO_TRASF'} );
						
						/* add 06/10/2020 - richiesta 56 - AZ_FORZA_RIENTRO_MAG_INTERVENTO_TRASF */
						console.log('arrAzForzaRientroMagInterventoTrasfFull',arrAzForzaRientroMagInterventoTrasfFull);
						
						
						/* add 06/10/2020 - richiesta 56 - AZ_FORZA_RIENTRO_MAG_INTERVENTO_TRASF */
						
						if (arrAzForzaRientroMagInterventoTrasfFull.length > 0) {
							
							/* aggiungo l'azione all arrAzPossibili */
							
							console.log('TRASFERIMENTO -> AZ_RIFIUTATO');
							
							item.TRASFERIMENTO_AZ_FORZA_RIENTRO_MAG_INTERVENTO	=	arrAzForzaRientroMagInterventoTrasfFull.length;
							
	
							item.arrAzPossibili.push({ 	context	:	'TRASFERIMENTO',
														value	:	'FORZA_RIENTRO_MAG_INTERVENTO',
														label	:	'Forza Rientro Magazzino Intervento',
														nMat	:	arrAzForzaRientroMagInterventoTrasfFull.length,
														padre	:	'TRASFERIMENTO_FULL',
														tabella	:	'TA_TRASFERIMENTO',
														filterTabella	: {STATO : 'CHIUSA,CORRIERE'}, /* NOT IN */
														plsql	: 	'ForzaRientroMagIntervTrasf'
													});
						}							
						
						
					} /* TRASFERIMENTO - Fine */
					
					item.mostraLoaderCheck = false;
					
					/**/
					if (item.arrAzPossibili.length>0) {
						/* row.arrAzPossibili = _.uniq(row.arrAzPossibili,'value');*/
						console.log('arrAzPossibili',item.arrAzPossibili);
						console.log('item',item);
						item.erroreCheckActionIdPolarys = false;
					} else {
						console.log('Nessuna azione possibile');
						item.erroreCheckActionIdPolarys = true;
					}
				
				});	

			
			});
		
		});
		
	});
	
	
};
	
	
	
	
		// funzione di export tabella
$scope.exportToExcel = function(){
	
	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	
	$scope.mostraLoaderUiModal = true;
	
	var confHeader	= {};
	var dataValue		=	{};
	var nomeFile 		= '';
	var nomeSheet		= '';
	
	
		
	nomeSheet = 'ATTIVITA';
	
	nomeFile	= $scope.configMainMenu[$scope.stateParamsUI.section].name;
	
	angular.forEach($scope.configGridUI, function(value,key) {
		console.log(key);
		console.log(value);
		if ((key !=='FUNCTION') && value.visible === true){
			confHeader[key] = {name : value.name, id : value.name, label : value.label, visible : true, enabled : false, type:'string' };
		}
	
	});
	
	dataValue = $scope.data;
	
	// call services macrotasks


		var data = [];
		// faccio logica per definire il set dati da printare
		var dateNow = new Date();
		dateNow = $filter('date')(dateNow,'yyyyMMddHHmmss');
	
		var obj = {
				header	:	confHeader,
				data		:	dataValue
		};
		// ciclo su di esso
		obj = filterNumbers(filterDate(obj));
		console.log(obj);
		_.each(obj.data, function(row) {
			// costruzione nuovo oggetto per la stampa
			var printObj = {};
			_.each(row,function(val,key){
				// restituisce la row modificata con le sole chiavi con visibilita a true
				//console.log(confHeader[key]);
				
				if((confHeader[key]) && (confHeader[key].visible)){ printObj[confHeader[key].label] = val; }
			});
			// add all'array 
			data.push(printObj);
		});

		var ws =  XLSX.utils.json_to_sheet (data);
		// costruttore obj excel
		var wb =  XLSX.utils.book_new ();
		// appendo gli elementi al file
		XLSX.utils.book_append_sheet (wb, ws, nomeSheet );
		// scrivo il file
		XLSX.writeFile(wb, nomeFile+'_'+dateNow+'.xlsx');

		$scope.mostraLoaderUiModal = false;
		
		// jscs:enable requireCamelCaseOrUpperCaseIdentifiers

};	
	
	
	/* cambia pagina */ 
	$scope.pageChanged = function() {
		var startPos = ($scope.currentPage - 1) * $scope.itemsPerPage;
		return startPos;
	};
	
	/*$scope.viewActivity = function(item){
		// restituisce l'URL dinamico creato da
		var urlMappa =  ActivityViewServices.getUrl({
			'stile':'custom_rma_sirti',
			'id_attivita_passata':item.ID_ATTIVITA
		});
		$window.open(urlMappa,'_blank','location=no');
	};*/
	
	$scope.viewActivity = function(item){
		// restituisce l'URL dinamico creato da
		var urlMappa =  function(){
			var urlPolaris = ActivityViewServices.getUrl({
				'stile':'custom_rma_sirti',//FIXME dedicare un classe lato polaris
				'id_attivita_passata':item.ID_ATTIVITA
			});
		
			return urlPolaris;
		};
		
		//$window.open(urlMappa,'_blank','location=no');
		var urlActivity = {src: urlMappa(), title:'ACTIVITY ID : '+item.ID_ATTIVITA+' '};
	
		/* UI MODALE - Inizio */
		var $ctrl = this;
		
		$ctrl.animationsEnabled = true;
		$ctrl.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab,urlActivity) {
			/*var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;*/
			var modalInstance = $uibModal.open({
				animation: $ctrl.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/common/activityViewModal.html',
				controller: 'ActivityViewModalCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						itemSelected: function () { return itemSelected; },
						uiChiamante: function () { return uiChiamante; },
						uiConfig: function () { return uiConfig; },
						tab: function () { return tab; },
						givenUrl:function(){return urlActivity;}
				}
			});
			modalInstance.result.then(function (selectedItem) {
				$ctrl.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
				$scope.getGrid();
				
			});
		};
		$ctrl.open('fullscreen',null,item, $scope.configMainMenu[$scope.stateParamsUI.section] ,$scope.configModalUI,'',urlActivity);
	};
	
	
	/* UI MODALE AZIONE - Inizio */	
	
	$scope.apriModaleAzione = function (itemSelected,azioneAttivitaSelected,keySelected){
		

		
		var $ctrl = this;
		
		$ctrl.animationsEnabled = true;
		
		$ctrl.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab,azioneAttivitaSelected,keySelected) {
			/*var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;*/

			
			var modalInstance = $uibModal.open({
				animation: $ctrl.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/uiPmAndAssTecnici/modalAzioneUI.html',
				controller: 'modalAzioneCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						itemSelected: function () {
							return itemSelected;
						},
						uiChiamante: function () {
							return uiChiamante;
						},
						uiConfig: function () {
							return uiConfig;
						},
						tab: function () {
							return tab;
						},
						azioneAttivitaSelected: function () {
							return azioneAttivitaSelected;
						},
						keySelected: function () {
							return keySelected;
						}
				}
			});
		
			modalInstance.result.then(function (selectedItem) {
				$ctrl.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
				$scope.getGrid();

			});
		};
		
		
		$ctrl.open('fullscreen',null,itemSelected,$scope.configMainMenu[$scope.stateParamsUI.section],$scope.configModalUI,'',azioneAttivitaSelected,keySelected);
	};
	
	/* UI MODALE INFO - Inizio */
	
	
	$scope.apriModaleInfo = function (itemSelected,objInfoButtonModalSelected,keySelected){
		

		
		var $ctrl = this;
		
		$ctrl.animationsEnabled = true;
		
		$ctrl.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab,objInfoButtonModalSelected,keySelected) {
			/*var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;*/

			
			var modalInstance = $uibModal.open({
				animation: $ctrl.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/uiPmAndAssTecnici/modalInfoUI.html',
				controller: 'modalInfoCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						itemSelected: function () {
							return itemSelected;
						},
						uiChiamante: function () {
							return uiChiamante;
						},
						uiConfig: function () {
							return uiConfig;
						},
						tab: function () {
							return tab;
						},
						objInfoButtonModalSelected: function () {
							return objInfoButtonModalSelected;
						},
						keySelected: function () {
							return keySelected;
						}
				}
			});
		
			modalInstance.result.then(function (objRet) {
				//$ctrl.selected = selectedItem;
				console.log('objRet: ',objRet);

				$scope.apriModaleAzione(objRet.item,objRet.azioneAttivita,objRet.key);

			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
								
				$scope.getGrid();

			});
		};
		
		
		$ctrl.open('fullscreen',null,itemSelected,$scope.configMainMenu[$scope.stateParamsUI.section],$scope.configModalUI,'',objInfoButtonModalSelected,keySelected);
	};	

}


angular.module('guiMtzNocMasterV2').controller('gsUiPmAndAssTecniciCtrl', gsUiPmAndAssTecniciCtrl);
