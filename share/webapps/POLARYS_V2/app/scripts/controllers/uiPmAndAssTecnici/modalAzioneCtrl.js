
'use strict';

/* Controller della Form Modale che verra' chiamata da tutte le UI */

function modalAzioneCtrl($scope,$rootScope,$uibModalInstance,itemSelected,uiChiamante,uiConfig,tab,azioneAttivitaSelected,keySelected,
Alert,$window,_ , $timeout,filterDate,filterNumbers,getTaOrdineSpedizioneNoBufferGrid,getTaStaffettaNoBufferGrid,getTaDicUsoNoBufferGrid,
azInsertMtzAppMobileReqBufferMassive,getTaOrdineSpedizioneCorriereGrid,getTaStaffettaCorriereGrid,ActivityViewServices,$uibModal, $log,
getTaTrasferimentoNoBufferGrid,$filter,$q){
	/*jshint validthis: true */
	/* global XLSX */
	// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
	
	$scope.$ctrl 	= this;
	var oriCtrl		={};
	
	$scope.$ctrl.alert											= Alert;
	$scope.$ctrl.mostraLoaderUiModal				= false;
	$scope.$ctrl.mostraLoaderAutocomplete		= false;
	$scope.$ctrl.collapseSummary						= false;
	$scope.$ctrl.showInfoErrorGrid 					= false;

	$scope.$ctrl.item							= itemSelected;
	$scope.$ctrl.chiamante				= uiChiamante;
	$scope.$ctrl.config						= uiConfig;
	$scope.$ctrl.cssSmall					= 'small';
	$scope.$ctrl.tab							= tab;
	$scope.$ctrl.azioneAttivita 	= azioneAttivitaSelected;
	$scope.$ctrl.colonnaOperativa	= keySelected;
	
	$scope.$ctrl.config.DICHIARAZIONE_USO.visibleCol = false;
	
	
	$scope.$ctrl.disabledConferma	=	true;
	$scope.$ctrl.nascondiConferma = false;

	$scope.$ctrl.actionSelected = {
		actionObj				: {},
		listaMateriali	: [],
		objInputVAlue 	: {}
	};
	
	$scope.$ctrl.buttonSceltaConfermaCfg	= [
			{
				name			: 'SI',
				label 		: 'SI',
				disabled	: false,
				icon			: 'fa fa-check',
				value			: 'SI',
				tooltip		: 'SI',
				css				: 'btn-dropbox'
			},
			{
				name			: 'NO',
				label 		: 'NO',
				disabled	: false,
				icon			: 'fa fa-ban',
				value			: 'NO',
				tooltip		: 'NO',
				css				: 'btn-linkedin'
			}
		];
	
	if (($scope.$ctrl.azioneAttivita.tabella === 'TA_DICHIARAZIONE_USO_MATERIALE') && ($scope.$ctrl.azioneAttivita.value === 'FORZA_RIENTRO_MAG_INTERVENTO')) {
		$scope.$ctrl.buttonDicUsoCfg	= [
			{
				name			: 'NON_UTILIZZATO',
				label 		: 'Non Utilizzato',
				disabled	: true,
				icon			: 'fa fa-thumbs-down',
				value			: 'Non Utilizzato',
				tooltip		: 'Causale dichiarazione uso: Non Utilizzato',
				css				: 'btn-linkedin'
			}
		];	
	} else {
		$scope.$ctrl.buttonDicUsoCfg	= [
			{
				name			: 'INSTALLATO',
				label 		: 'Installato',
				disabled	: true,
				icon			: 'fa fa-thumbs-up',
				value			: 'Installato',
				tooltip		: 'Causale dichiarazione uso: Installato',
				css				: 'btn-dropbox'
			},
			{
				name			: 'NON_UTILIZZATO',
				label 		: 'Non Utilizzato',
				disabled	: true,
				icon			: 'fa fa-thumbs-down',
				value			: 'Non Utilizzato',
				tooltip		: 'Causale dichiarazione uso: Non Utilizzato',
				css				: 'btn-linkedin'
			},
			{
				name			: 'GUASTO',
				label 		: 'Guasto',
				disabled	: true,
				icon			: 'fa fa-wrench',
				value			: 'Guasto',
				tooltip		: 'Causale dichiarazione uso: Guasto',
				css				: 'btn-dropbox'
			},
			{
				name			: 'NON_CONFORME',
				label 		: 'Non Conforme',
				disabled	: true,
				icon			: 'fa fa-tag',
				value			: 'Non Conforme',
				tooltip		: 'Causale dichiarazione uso: Non Conforme',
				css				: 'btn-linkedin'
			}
	
		];		
	}
	
	$scope.$ctrl.checkDicUsoDaFile = 'GUI';
	/* resetto sempre il campo di input file */
	$scope.$ctrl.fileExcel = {};
	$scope.$ctrl.aErrorImportExcel = undefined;
	$scope.$ctrl.msgErrorImport = '';
	$scope.$ctrl.dataValueExportExcel = [];
		
	if (angular.element('#fileExcelDicUsoUpload')[0]) {
		angular.element('#fileExcelDicUsoUpload')[0].value	= '';
	}
	

	//$scope.$ctrl.disabledConferma	=	true;
	
	$scope.$ctrl.disabilitaEditCustom = {

	};
	
	
	console.log('item',$scope.$ctrl.item);
	console.log('azioneAttivita',$scope.$ctrl.azioneAttivita);
	console.log('colonnaOperativa',$scope.$ctrl.colonnaOperativa);
	
	console.log('chiamante',$scope.$ctrl.chiamante);
	console.log('config',$scope.$ctrl.config);

	$scope.$ctrl.selectAll = false;
	$scope.$ctrl.activitiesSelected = [];

	// checked All activities
	$scope.$ctrl.checkedAll = function(){
		// ckeck di tutti gli elementi
		console.log('qui');
		_.each( _.where( $scope.$ctrl.listaAttivitaMassive) , function(att){
			att.SELECTED = $scope.$ctrl.selectAll;
		});
		if($scope.$ctrl.options){
			$scope.$ctrl.options.global = '';
		}
		checkElement();
	};
	
	// funzione PRIVATA di check degli elementi
	var checkElement = function(){
		$scope.$ctrl.activitiesSelected = [];
		$scope.$ctrl.disabledConferma = true;
		
		/* disabilito tutti i pulsanti custom della dic uso */
		if ($scope.$ctrl.azioneAttivita.tabella === 'TA_DICHIARAZIONE_USO_MATERIALE'){
			_.each( _.where($scope.$ctrl.buttonDicUsoCfg , { disabled : false } ) , function(btn){
				btn.disabled = true;
			});
		}
		
		_.each( _.where($scope.$ctrl.listaAttivitaMassive , { SELECTED : true } ) , function(att){
			$scope.$ctrl.activitiesSelected.push(att);
		});
		// attiva il options.selectAll
		if( $scope.$ctrl.activitiesSelected.length > 0 ){
			$scope.$ctrl.selectAll = true;
			$scope.$ctrl.disabledConferma = false;
			
			/* abilito tutti i pulsanti custom della dic uso */
			if ($scope.$ctrl.azioneAttivita.tabella === 'TA_DICHIARAZIONE_USO_MATERIALE'){
				_.each( _.where($scope.$ctrl.buttonDicUsoCfg , { disabled : true } ) , function(btn){
					btn.disabled = false;
				});
			}
		}
	};

	// aggiunge su un array temporaneo
	$scope.$ctrl.addActivitiesSelected = function(){
		checkElement();
		if(angular.isDefined($scope.$ctrl.activitiesSelected)){
			if($scope.$ctrl.activitiesSelected.length === $scope.$ctrl.listaAttivitaMassive.length){
				$scope.$ctrl.selectAll = true;
				$scope.$ctrl.disabledConferma = false;
				
				/* abilito tutti i pulsanti custom della dic uso */
				if ($scope.$ctrl.azioneAttivita.tabella === 'TA_DICHIARAZIONE_USO_MATERIALE'){
					_.each( _.where($scope.$ctrl.buttonDicUsoCfg , { disabled : true } ) , function(btn){
						btn.disabled = false;
					});
				}				
				
			}
		}
		
	};
	
	
	/* gestione della sezione excel */
	
	$scope.$ctrl.clickDicUsoDaGuiFile = function () {
		$scope.$ctrl.clickAnnullaImportExcel();
		$scope.$ctrl.getGridMassiva();
	};
	
	/* gestisco apri/chiudi file upload excel */
	$scope.$ctrl.clickAnnullaImportExcel = function () {
		
		/* resetto sempre il campo di input file */
		$scope.$ctrl.fileExcel = {};
		$scope.$ctrl.aErrorImportExcel = undefined;
		$scope.$ctrl.msgErrorImport = '';
		
		$scope.$ctrl.selectAll = false;
		$scope.$ctrl.activitiesSelected = [];
		$scope.$ctrl.listaAttivitaMassive = [];
			
		if (angular.element('#fileExcelDicUsoUpload')[0]) {
			angular.element('#fileExcelDicUsoUpload')[0].value	= '';
		}
	
	};
	
/* export tamplate axcel */


$scope.$ctrl.exportTemplateExcel = function() {

    var confHeader = {
            ID_POLARYS				:{ name : 'ID_POLARYS', id : 'ID_POLARYS', label : 'ID_POLARYS', visible : true, enabled : false, type:'string' },
            ID_SIRTI				:{ name : 'ID_SIRTI', id : 'ID_SIRTI', label : 'ID_SIRTI', visible : true, enabled : false, type:'string' },
			CLASSIFICAZIONE			:{ name : 'CLASSIFICAZIONE', id : 'CLASSIFICAZIONE', label : 'CLASSIFICAZIONE', visible : true, enabled : false, type:'string' },
            PART_NUMBER				:{ name : 'PART_NUMBER', id : 'PART_NUMBER', label : 'PART_NUMBER', visible : true, enabled : false, type:'string' },
            SERIAL_NUMBER			:{ name : 'SERIAL_NUMBER', id : 'SERIAL_NUMBER', label : 'SERIAL_NUMBER', visible : true, enabled : false, type:'string' },
            QUANTITA				:{ name : 'QUANTITA', id : 'QUANTITA', label : 'QUANTITA', visible : true, enabled : false, type:'string' },
        };
        
        
    var dataValue = [];
	
	angular.forEach($scope.$ctrl.dataValueExportExcel, function(mat) {
		dataValue.push( {
            ID_POLARYS				: mat.ID_ATTIVITA_MASTER,
            ID_SIRTI				: mat.ID_SIRTI,
			CLASSIFICAZIONE			: mat.CLASSIFICAZIONE_MATERIALE,
            PART_NUMBER				: mat.PART_NUMBER,
            SERIAL_NUMBER			: mat.SERIAL_NUMBER,
            QUANTITA				: mat.QUANTITA,
			});
        
	});
	
    var nomeFile = 'IMPORT_DIC_USO_DA_FILE';
    
    
    // call services macrotasks
    $scope.$ctrl.mostraLoaderUiModal = true;

        var data = [];
        // faccio logica per definire il set dati da printare
        var dateNow = new Date();
        dateNow = $filter('date')(dateNow,'yyyyMMddHHmmss');
    
        var obj = {
                header	:	confHeader,
                data		:	dataValue
        };
        // ciclo su di esso
        obj = filterNumbers(filterDate(obj));
        console.log(obj);
        _.each(obj.data, function(row) {
            // costruzione nuovo oggetto per la stampa
            var printObj = {};
            _.each(row,function(val,key){
                // restituisce la row modificata con le sole chiavi con visibilita a true
                //console.log(confHeader[key]);
                
                if((confHeader[key]) && (confHeader[key].visible)){ printObj[confHeader[key].label] = val; }
            });
            // add all'array 
            data.push(printObj);
        });

        var ws =  XLSX.utils.json_to_sheet (data);
        // costruttore obj excel
        var wb =  XLSX.utils.book_new ();
        // appendo gli elementi al file
        XLSX.utils.book_append_sheet (wb, ws, nomeFile );
        // scrivo il file
        XLSX.writeFile(wb, nomeFile+'_'+dateNow+'.xlsx');

        $scope.$ctrl.mostraLoaderUiModal = false; 

	};
	
	
	/*  export esito */
	$scope.$ctrl.exportEsitoImportExcel = function() {
	
		var confHeader = {
				ID_POLARYS				:{ name : 'ID_POLARYS', id : 'ID_POLARYS', label : 'ID_POLARYS', visible : true, enabled : false, type:'string' },
				ID_SIRTI				:{ name : 'ID_SIRTI', id : 'ID_SIRTI', label : 'ID_SIRTI', visible : true, enabled : false, type:'string' },
				CLASSIFICAZIONE			:{ name : 'CLASSIFICAZIONE', id : 'CLASSIFICAZIONE', label : 'CLASSIFICAZIONE', visible : true, enabled : false, type:'string' },
				PART_NUMBER				:{ name : 'PART_NUMBER', id : 'PART_NUMBER', label : 'PART_NUMBER', visible : true, enabled : false, type:'string' },
				SERIAL_NUMBER			:{ name : 'SERIAL_NUMBER', id : 'SERIAL_NUMBER', label : 'SERIAL_NUMBER', visible : true, enabled : false, type:'string' },
				QUANTITA				:{ name : 'QUANTITA', id : 'QUANTITA', label : 'QUANTITA', visible : true, enabled : false, type:'string' },
				ESITO	        		:{ name : 'ESITO', id : 'ESITO', label : 'ESITO', visible : true, enabled : false, type:'string' }
			};
			
			
		var dataValue = $scope.$ctrl.aErrorImportExcel;
		
		var nomeFile = 'ERRORI_IMPORT_DIC_USO_DA_FILE';
		
		
		// call services macrotasks
		$scope.$ctrl.mostraLoaderUiModal = true;
	
			var data = [];
			// faccio logica per definire il set dati da printare
			var dateNow = new Date();
			dateNow = $filter('date')(dateNow,'yyyyMMddHHmmss');
		
			var obj = {
					header	:	confHeader,
					data		:	dataValue
			};
			// ciclo su di esso
			obj = filterNumbers(filterDate(obj));
			console.log(obj);
			_.each(obj.data, function(row) {
				// costruzione nuovo oggetto per la stampa
				var printObj = {};
				_.each(row,function(val,key){
					// restituisce la row modificata con le sole chiavi con visibilita a true
					//console.log(confHeader[key]);
					
					if((confHeader[key]) && (confHeader[key].visible)){ printObj[confHeader[key].label] = val; }
				});
				// add all'array 
				data.push(printObj);
			});
	
			var ws =  XLSX.utils.json_to_sheet (data);
			// costruttore obj excel
			var wb =  XLSX.utils.book_new ();
			// appendo gli elementi al file
			XLSX.utils.book_append_sheet (wb, ws, nomeFile );
			// scrivo il file
			XLSX.writeFile(wb, nomeFile+'_'+dateNow+'.xlsx');
	
			$scope.$ctrl.mostraLoaderUiModal = false; 

	};
	
	
	/* procedura che gestisce l'import da excel lato GUI */
	$scope.$ctrl.clickCaricaExcel = function () {
		
		console.log('clickCaricaExcel');
		
		$scope.$ctrl.mostraLoaderUiModal = true;
		$scope.$ctrl.aErrorImportExcel = [];
		$scope.$ctrl.msgErrorImport = '';
		
		$scope.$ctrl.selectAll = false;
		$scope.$ctrl.activitiesSelected = [];
		$scope.$ctrl.listaAttivitaMassive = [];
		
		
		/* gestisco se il file e' pieno */
		if ($scope.$ctrl.fileExcel.data){
			
			/* controllo se ci sono le colonne */
			if ($scope.$ctrl.fileExcel.columnDefs){
				
				if ($scope.$ctrl.fileExcel.columnDefs.length === 6){
			
					/* controllo se le colonne sono valide */
					var contaError = 0;
					angular.forEach($scope.$ctrl.fileExcel.columnDefs, function(colonna) {
						console.log('colonna.field: ',colonna.field)
						if (colonna.field !== 'ID_POLARYS'){
							if (colonna.field !== 'ID_SIRTI'){
								if (colonna.field !== 'CLASSIFICAZIONE'){
									if (colonna.field !== 'PART_NUMBER'){
										if (colonna.field !== 'SERIAL_NUMBER'){
											if (colonna.field !== 'QUANTITA'){
												contaError++;
											}
										}
									}
								}
							}
						}
					});
					
					/* proseguo con il contenuto del file */
					if (contaError === 0){
						
						if ($scope.$ctrl.fileExcel.data.length > 0 ) {
							
							$scope.$ctrl.aErrorImportExcel = [];
							contaError = 0;
							
							angular.forEach($scope.$ctrl.fileExcel.data, function(riga) {
								
								console.log('riga: ',riga);
								console.log('parseInt QUANTITA: ',parseInt(riga.QUANTITA));
								
								var checkObj = {};
								
								if (	((riga.ID_POLARYS)		&& (riga.ID_POLARYS !== '')  && (parseInt(riga.ID_POLARYS)) && (parseInt(riga.ID_POLARYS) === parseInt($scope.$ctrl.item.ID_ATTIVITA)) )	&& 
										((riga.ID_SIRTI) 		&& (riga.ID_SIRTI !== '')) 		&&
										((riga.CLASSIFICAZIONE)	&& (riga.CLASSIFICAZIONE !== '')) 		&&							
										((riga.PART_NUMBER)		&& (riga.PART_NUMBER !== ''))	&& 
										(
										 (((riga.SERIAL_NUMBER)	&& (riga.SERIAL_NUMBER !== '')) && (riga.CLASSIFICAZIONE === 'NON CONSUMABILE')) ||
										 (((!riga.SERIAL_NUMBER)	|| (riga.SERIAL_NUMBER === '')) && (riga.CLASSIFICAZIONE === 'CONSUMABILE'))
										 )	&&
										((riga.QUANTITA) 		&& (riga.QUANTITA !== '') && (
											((riga.CLASSIFICAZIONE === 'NON CONSUMABILE') && (parseInt(riga.QUANTITA)) && (parseInt(riga.QUANTITA) == 1)) ||
											((riga.CLASSIFICAZIONE === 'CONSUMABILE') && (parseInt(riga.QUANTITA)) && (parseInt(riga.QUANTITA) >= 1)) 
											)
										 )
	
									) {
									
									checkObj = {
										ID_POLARYS		:	riga.ID_POLARYS,
										ID_SIRTI		:	riga.ID_SIRTI,
										CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
										PART_NUMBER		:	riga.PART_NUMBER,
										SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
										QUANTITA		:	riga.QUANTITA,
										ESITO			:	'Record Consistente'
									};
									
								} else {
									contaError++;

									if ((!riga.ID_SIRTI) || (riga.ID_SIRTI === '')) {
										
										checkObj = {
											ID_POLARYS		:	riga.ID_POLARYS,
											ID_SIRTI		:	riga.ID_SIRTI,
											CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
											PART_NUMBER		:	riga.PART_NUMBER,
											SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
											QUANTITA		:	riga.QUANTITA,	
											ESITO			:	'Record Errato: Colonna ID_SIRTI non valorizzata'
										};   
	
									}
									else if ((!riga.ID_POLARYS) || (riga.ID_POLARYS === '') || ((riga.ID_POLARYS) && (parseInt(riga.ID_POLARYS)) && (parseInt(riga.ID_POLARYS) !== parseInt($scope.$ctrl.item.ID_ATTIVITA)))) {
										
										checkObj = {
											ID_POLARYS		:	riga.ID_POLARYS,
											ID_SIRTI		:	riga.ID_SIRTI,
											CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
											PART_NUMBER		:	riga.PART_NUMBER,
											SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
											QUANTITA		:	riga.QUANTITA,	
											ESITO			:	'Record Errato: Colonna ID_POLARYS non valorizzata o ID_SIRTI appartenete ad altro ID_POLARYS. Verificare che il valore di ID_POLARYS del file caricato sia uguale a: '+$scope.$ctrl.item.ID_ATTIVITA
										};   
	
									}
									else if ((!riga.CLASSIFICAZIONE) || (riga.CLASSIFICAZIONE === '')) {
										
										checkObj = {
											ID_POLARYS		:	riga.ID_POLARYS,
											ID_SIRTI		:	riga.ID_SIRTI,
											CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
											PART_NUMBER		:	riga.PART_NUMBER,
											SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
											QUANTITA		:	riga.QUANTITA,	
											ESITO			:	'Record Errato: Colonna CLASSIFICAZIONE non valorizzata'
										};  
	
									}
									else if ((!riga.PART_NUMBER) || (riga.PART_NUMBER === '')) {
										
										checkObj = {
											ID_POLARYS		:	riga.ID_POLARYS,
											ID_SIRTI		:	riga.ID_SIRTI,
											CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
											PART_NUMBER		:	riga.PART_NUMBER,
											SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
											QUANTITA		:	riga.QUANTITA,	
											ESITO			:	'Record Errato: Colonna PART_NUMBER non valorizzata'
										};  
	
									}
									
									else if ((riga.CLASSIFICAZIONE === 'NON CONSUMABILE') && ((!riga.SERIAL_NUMBER) || (riga.SERIAL_NUMBER === '')) ) {
										
										checkObj = {
											ID_POLARYS		:	riga.ID_POLARYS,
											ID_SIRTI		:	riga.ID_SIRTI,
											CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
											PART_NUMBER		:	riga.PART_NUMBER,
											SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
											QUANTITA		:	riga.QUANTITA,	
											ESITO			:	'Record Errato: materiale NON CONSUMABILE, la Colonna SERIAL_NUMBER deve essere valorizzata'
										};   
	
									}
									else if ((riga.CLASSIFICAZIONE === 'CONSUMABILE') && ((riga.SERIAL_NUMBER) && (riga.SERIAL_NUMBER !== '')) ) {
										
										checkObj = {
											ID_POLARYS		:	riga.ID_POLARYS,
											ID_SIRTI		:	riga.ID_SIRTI,
											CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
											PART_NUMBER		:	riga.PART_NUMBER,
											SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
											QUANTITA		:	riga.QUANTITA,	
											ESITO			:	'Record Errato: materiale CONSUMABILE, la Colonna SERIAL_NUMBER non deve essere valorizzata'
										};   
	
									}
									else if ( (!riga.QUANTITA) || (riga.QUANTITA === '') ) {
										
										checkObj = {
											ID_POLARYS		:	riga.ID_POLARYS,
											ID_SIRTI		:	riga.ID_SIRTI,
											CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
											PART_NUMBER		:	riga.PART_NUMBER,
											SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
											QUANTITA		:	riga.QUANTITA,	
											ESITO			:	'Record Errato: Colonna QUANTITA non valorizzata'
										};   
	
									}
									
									else if ((riga.CLASSIFICAZIONE === 'NON CONSUMABILE') && ((parseInt(riga.QUANTITA) > 1) || (parseInt(riga.QUANTITA) <= 0)))  {
										
										checkObj = {
											ID_POLARYS		:	riga.ID_POLARYS,
											ID_SIRTI		:	riga.ID_SIRTI,
											CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
											PART_NUMBER		:	riga.PART_NUMBER,
											SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
											QUANTITA		:	riga.QUANTITA,	
											ESITO			:	'Record Errato: materiali NON CONSUMABILE la colonna QUANTITA deve essere sempre uguale a 1'
										};   
	
									}
									else if ((riga.CLASSIFICAZIONE === 'CONSUMABILE') && (parseInt(riga.QUANTITA) <= 0))  {
										
										checkObj = {
											ID_POLARYS		:	riga.ID_POLARYS,
											ID_SIRTI		:	riga.ID_SIRTI,
											CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
											PART_NUMBER		:	riga.PART_NUMBER,
											SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
											QUANTITA		:	riga.QUANTITA,	
											ESITO			:	'Record Errato: materiali CONSUMABILE la colonna QUANTITA deve essere sempre maggiore di 0'
										};   
	
									} 
   
	
									else {
										console.log('altro errore non gestito');
									}
								}
								
								 $scope.$ctrl.aErrorImportExcel.push(checkObj);
								
							});
							
							/* proseguo con il contenuto del file */
							if (contaError === 0){
								
								$scope.$ctrl.aErrorImportExcel = [];
								
						
								var deferred = $q.defer();
								var urlCalls = [];
								
								var contaOK = 0;
								var contaKO = 0;
								

                                var errObj = {};
								var esitoImport;
	
									
								angular.forEach($scope.$ctrl.fileExcel.data, function(riga) {
								
								
									/* parametri procedura salvataggio cfg */
									var checkTmpParams =	{
																filter_ID_ATTIVITA_MASTER_equals		:	riga.ID_POLARYS		,
																filter_ID_SIRTI_equals					:	riga.ID_SIRTI		,
																filter_PART_NUMBER_equals	    		:	riga.PART_NUMBER	,
																filter_CLASSIFICAZIONE_MATERIALE_equals	:	riga.CLASSIFICAZIONE,
																filter_SERIAL_NUMBER_equals				:	riga.CLASSIFICAZIONE === 'NON CONSUMABILE' ? riga.SERIAL_NUMBER : null ,
																filter_QUANTITA_equals  				:	riga.CLASSIFICAZIONE === 'NON CONSUMABILE' ? 1	: riga.QUANTITA		
															};
									
									console.log('checkTmpParams: ',checkTmpParams);
									
									/* eseguo la procedura di salvataggio della cfg */
									console.log('getTaDicUsoNoBufferGrid');
									
									esitoImport = getTaDicUsoNoBufferGrid.getData(checkTmpParams).$promise.then(function(result) {
										
										errObj = {};
										
										
										console.log(result);
										if (result.data.results && result.data.results.length > 0){
											
											var giaGestita = false;
											_.each( _.where($scope.$ctrl.listaAttivitaMassive , {
												
													ID_ATTIVITA_MASTER				:	result.data.results[0].ID_ATTIVITA_MASTER,
													ID_SIRTI						:	result.data.results[0].ID_SIRTI,
													CLASSIFICAZIONE_MATERIALE		:	result.data.results[0].CLASSIFICAZIONE_MATERIALE,
													PART_NUMBER						:	result.data.results[0].PART_NUMBER,
													SERIAL_NUMBER					:	result.data.results[0].SERIAL_NUMBER,
													QUANTITA						:	result.data.results[0].QUANTITA,													
												
												} ) , function(att){
												//$scope.$ctrl.activitiesSelected.push(att);
												console.log('gia gestita: ',att);
												giaGestita = true;
											});
											
											if (!giaGestita) {
												//$scope.$ctrl.alert.success('Configurazione salvata con successo', { ttl: 10000 });
													errObj = {
														ID_POLARYS		:	riga.ID_POLARYS,
														ID_SIRTI		:	riga.ID_SIRTI,
														CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
														PART_NUMBER		:	riga.PART_NUMBER,
														SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
														QUANTITA		:	riga.QUANTITA,	
														ESITO_FUNZ		:	'OK' ,
														ESITO			:	'ID_SIRTI Valido'
													};
												
												result.data.results[0].SELECTED = false;
												$scope.$ctrl.listaAttivitaMassive.push(result.data.results[0]);
											} else {
												//$scope.$ctrl.alert.success('Configurazione salvata con successo', { ttl: 10000 });
													errObj = {
														ID_POLARYS		:	riga.ID_POLARYS,
														ID_SIRTI		:	riga.ID_SIRTI,
														CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
														PART_NUMBER		:	riga.PART_NUMBER,
														SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
														QUANTITA		:	riga.QUANTITA,	
														ESITO_FUNZ		:	'KO' ,
														ESITO			:	'ID_SIRTI Valido ma duplicato nel file'
													};
												
												//result.data.results[0].SELECTED = false;
												//$scope.$ctrl.listaAttivitaMassive.push(result.data.results[0]);	
											}
											

												
											
											return errObj;
									
										} else {
											//$scope.$ctrl.alert.error('Errore Salvataggio: '+resultAz.data.DESC_ESITO, { ttl: 10000 });
												errObj = {
													ID_POLARYS		:	riga.ID_POLARYS,
													ID_SIRTI		:	riga.ID_SIRTI,
													CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
													PART_NUMBER		:	riga.PART_NUMBER,
													SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
													QUANTITA		:	riga.QUANTITA,	
													ESITO_FUNZ		:	'KO' ,
													ESITO			:	'ID_SIRTI non trovato a sistema o appartenete ad altro ID_POLARYS'
												};
											
											return errObj;
										}
										
									
									},function(err) {
										console.log(err);
										//$scope.$ctrl.mostraLoaderUiModal = false;
										//$scope.$ctrl.alert.error(err, { ttl: 10000 });
										errObj = {
											ID_POLARYS		:	riga.ID_POLARYS,
											ID_SIRTI		:	riga.ID_SIRTI,
											CLASSIFICAZIONE	:	riga.CLASSIFICAZIONE,
											PART_NUMBER		:	riga.PART_NUMBER,
											SERIAL_NUMBER	:	riga.SERIAL_NUMBER,
											QUANTITA		:	riga.QUANTITA,	
											ESITO_FUNZ		:	'KO' ,
											ESITO			:	'Errore check ID_SIRTI nella V_POL2_TA_DIC_USO_NOBUFF_GRID'
										};
											
										return errObj;
									});   
									
									urlCalls.push(esitoImport);
									
								});
								
								$q.all(urlCalls)
								.then(
									function(results) {
										deferred.resolve();
								
										angular.forEach(results, function(esitoChiamata) {
											console.log('esitoChiamata',esitoChiamata);
											if (esitoChiamata.ESITO_FUNZ === 'KO'){
												contaKO++;
												$scope.$ctrl.aErrorImportExcel.push(esitoChiamata);
											}
											else{
												contaOK++;
											}
											
											
										});
										
										if (contaKO > 0){
											$scope.$ctrl.msgErrorImport = ''+contaKO+'/'+(contaKO+contaOK)+' Errori di importazione nel file: ';
										} else {
											$scope.$ctrl.msgErrorImport = 'Tutti i record sono stati verificati con successo';
											$scope.$ctrl.aErrorImportExcel = undefined;
										}
										
										
										if($scope.$ctrl.listaAttivitaMassive.length > 0){
											$scope.$ctrl.selectAll = true;
											$scope.$ctrl.checkedAll();
										}
										
								
										$scope.$ctrl.mostraLoaderUiModal	= false;
										$scope.$ctrl.fileExcel 						= {};
										
										//$scope.$ctrl.clickCaricaImportElaboratiGrid();
										
								
								},
								function(errors) {
									deferred.reject(errors);
									$scope.$ctrl.mostraLoaderUiModal = false;
								},
								function(updates) {
									deferred.update(updates);
									$scope.$ctrl.mostraLoaderUiModal = false;
								});	

									
							} else {
						   //$scope.$ctrl.alert.error('Il file ha intestazione delle colonne diversa da: CODICE_SIRTI_A,PART_NUMBER_A,CODICE_SIRTI_B,PART_NUMBER_B,COMPATIBILE,EQUIVALENTE', { ttl: 10000 });
						   $scope.$ctrl.msgErrorImport = 'Scartato: Il file contiene degli errori. Risolverli prima di caricare nuovamente il file';
						   $scope.$ctrl.mostraLoaderUiModal = false; 
						   }                                       
						} else {
						//$scope.$ctrl.alert.error('Il file ha intestazione delle colonne diversa da: CODICE_SIRTI_A,PART_NUMBER_A,CODICE_SIRTI_B,PART_NUMBER_B,COMPATIBILE,EQUIVALENTE', { ttl: 10000 });
						$scope.$ctrl.msgErrorImport = 'Scartato: Il file non contiene i materiali. Risolvere prima di caricare nuovamente il file';
						$scope.$ctrl.mostraLoaderUiModal = false; 
						}
					}
					else{
						//$scope.$ctrl.alert.error('Il file ha intestazione delle colonne diversa da: CODICE_SIRTI_A,PART_NUMBER_A,CODICE_SIRTI_B,PART_NUMBER_B,COMPATIBILE,EQUIVALENTE', { ttl: 10000 });
						$scope.$ctrl.msgErrorImport = 'Scartato: Il file ha intestazione delle colonne diversa da: ID_POLARYS, ID_SIRTI, CLASSIFICAZIONE, PART_NUMBER, SERIAL_NUMBER e QUANTITA. Risolvere prima di caricare nuovamente il file	';
						$scope.$ctrl.mostraLoaderUiModal = false;
					}
				
				}
				else{
					//$scope.$ctrl.alert.error('Il file non ha intestazione delle colonne: CODICE_SIRTI_A,PART_NUMBER_A,CODICE_SIRTI_B,PART_NUMBER_B,COMPATIBILE,EQUIVALENTE', { ttl: 10000 });
					$scope.$ctrl.msgErrorImport = 'Scartato: Il file non ha intestazione delle colonne: ID_POLARYS, ID_SIRTI, CLASSIFICAZIONE, PART_NUMBER, SERIAL_NUMBER e QUANTITA. Risolvere prima di caricare nuovamente il file	';
					$scope.$ctrl.mostraLoaderUiModal = false;
				}
			
			}
			else{
				//$scope.$ctrl.alert.error('Il file non ha intestazione delle colonne: CODICE_SIRTI_A,PART_NUMBER_A,CODICE_SIRTI_B,PART_NUMBER_B,COMPATIBILE,EQUIVALENTE', { ttl: 10000 });
				$scope.$ctrl.msgErrorImport = 'Scartato: Il file non ha intestazione delle colonne: ID_POLARYS, ID_SIRTI, CLASSIFICAZIONE, PART_NUMBER, SERIAL_NUMBER e QUANTITA. Risolvere prima di caricare nuovamente il file	';
				$scope.$ctrl.mostraLoaderUiModal = false;
			}
				
	
		}
		else {
			//$scope.$ctrl.alert.error('Il file &egrave; vuoto', { ttl: 10000 });
			$scope.$ctrl.msgErrorImport = 'Scartato: Il file &egrave; vuoto. Risolvere prima di caricare nuovamente il file';
			$scope.$ctrl.mostraLoaderUiModal = false;
		}
	
	};

	
	
	/* in base alla CFG del chiamante  determino come popolare la griglia delle attivita selezionate */


	$scope.$ctrl.getGridMassiva = function() {
		
		$scope.$ctrl.selectAll = false;
		$scope.$ctrl.activitiesSelected = [];
		//$scope.$ctrl.disableCbByActionSel = true;
		$scope.$ctrl.dataValueExportExcel = [];
		
		/* qui la GESTIONE_ATOMICA $scope.$ctrl.item */
		
		console.log('GESTIONE_ATOMICA',$scope.$ctrl.item.GESTIONE_ATOMICA);
		
		
		if ($scope.$ctrl.item.GESTIONE_ATOMICA == 'SI') {
			$scope.$ctrl.disableCbByActionSel = true;
			$scope.$ctrl.selectAll = false;
		}
		/* disabilito tutti i pulsanti custom della dic uso */
		if ($scope.$ctrl.azioneAttivita.tabella === 'TA_DICHIARAZIONE_USO_MATERIALE'){
			_.each( _.where($scope.$ctrl.buttonDicUsoCfg , { disabled : false } ) , function(btn){
				btn.disabled = true;
			});
		}		
		
		
		/* TA_ORDINE_SPEDIZIONE */
		
		if ($scope.$ctrl.azioneAttivita.tabella === 'TA_ORDINE_SPEDIZIONE'){
			
			$scope.$ctrl.nascondiConferma = false;
			
			var filterGridTaOrdSpedA = {};
			
			$scope.$ctrl.mostraLoaderUiModal = true;
			
			var aStatiSplit = $scope.$ctrl.azioneAttivita.filterTabella.STATO.split(',');
			
			console.log('aStatiSplit: ',aStatiSplit);
			
			if (aStatiSplit.length > 1) {
				/* solo per STATO e ID_ATTIVITA_MASTER */
				filterGridTaOrdSpedA = {
					filter_STATO_in									: $scope.$ctrl.azioneAttivita.filterTabella.STATO,
					filter_ID_ATTIVITA_MASTER_equals				: $scope.$ctrl.item.ID_ATTIVITA
				};
			} else {
				/* solo per STATO e ID_ATTIVITA_MASTER */
				filterGridTaOrdSpedA = {
					filter_STATO_equals								: $scope.$ctrl.azioneAttivita.filterTabella.STATO,
					filter_ID_ATTIVITA_MASTER_equals				: $scope.$ctrl.item.ID_ATTIVITA
				};				
			}
			

		
		
			getTaOrdineSpedizioneNoBufferGrid.getData(filterGridTaOrdSpedA).$promise.then(function(resultA) {
					
					console.log(resultA.data.results);
				
					var aFullResults =_.uniq(_.union(resultA.data.results),'ID_ATTIVITA');
						
					/* ciclo i valori trovati e ingetto il selectBox */
					
					angular.forEach(aFullResults, function(act) {
					
						console.log(act);
						
						if ($scope.$ctrl.item.GESTIONE_ATOMICA == 'SI') {
							act.SELECTED =  true;
							$scope.$ctrl.activitiesSelected.push(act);
						} else {
							act.SELECTED = false;	
						}
						
						
						if (''+$scope.$ctrl.item.ID_ATTIVITA === act.ID_ATTIVITA_MASTER){
							act.IS_MASTER_SELECTED = true;
						} else {
							act.IS_MASTER_SELECTED = false;
						}
						
						if ((!act.SERIAL_NUMBER) || (act.SERIAL_NUMBER === '') || (act.SERIAL_NUMBER === null) || (act.SERIAL_NUMBER === undefined)){
							act.CLASSIFICAZIONE_MATERIALE = 'CONSUMABILE';
						} else {
							act.CLASSIFICAZIONE_MATERIALE = 'NON CONSUMABILE';
						}
					
					});
					
					console.log(aFullResults);
					
					$scope.$ctrl.listaAttivitaMassive = _.sortBy(aFullResults,'IS_MASTER_SELECTED').reverse();
					
					$scope.$ctrl.mostraLoaderUiModal = false;
					
		
			});
		
		} /* fine TA_ORDINE_SPEDIZIONE */
		
		/* TA_STAFFETTA */
		
		if ($scope.$ctrl.azioneAttivita.tabella === 'TA_STAFFETTA'){
			
			$scope.$ctrl.nascondiConferma = false;
			
			var filterGridTaStaffettaA = {};
			
			$scope.$ctrl.mostraLoaderUiModal = true;
			
			var aStatiSplit = $scope.$ctrl.azioneAttivita.filterTabella.STATO.split(',');
			
			console.log('aStatiSplit: ',aStatiSplit);
			
			if (aStatiSplit.length > 1) {
				/* solo per STATO e ID_ATTIVITA_MASTER */
				filterGridTaStaffettaA = {
					filter_STATO_in									: $scope.$ctrl.azioneAttivita.filterTabella.STATO,
					filter_ID_ATTIVITA_MASTER_equals				: $scope.$ctrl.item.ID_ATTIVITA
				};
			} else {
				/* solo per STATO e ID_ATTIVITA_MASTER */
				filterGridTaStaffettaA = {
					filter_STATO_equals								: $scope.$ctrl.azioneAttivita.filterTabella.STATO,
					filter_ID_ATTIVITA_MASTER_equals				: $scope.$ctrl.item.ID_ATTIVITA
				};				
			}
		
		
			getTaStaffettaNoBufferGrid.getData(filterGridTaStaffettaA).$promise.then(function(resultA) {
					
					console.log(resultA.data.results);
				
					var aFullResults =_.uniq(_.union(resultA.data.results),'ID_ATTIVITA');
						
					/* ciclo i valori trovati e ingetto il selectBox */
					
					angular.forEach(aFullResults, function(act) {
					
						console.log(act);

						if ($scope.$ctrl.item.GESTIONE_ATOMICA == 'SI') {
							act.SELECTED =  true;
							$scope.$ctrl.activitiesSelected.push(act);
						} else {
							act.SELECTED = false;	
						}						
						
						
						if (''+$scope.$ctrl.item.ID_ATTIVITA === act.ID_ATTIVITA_MASTER){
							act.IS_MASTER_SELECTED = true;
						} else {
							act.IS_MASTER_SELECTED = false;
						}
						
						if ((!act.SERIAL_NUMBER) || (act.SERIAL_NUMBER === '') || (act.SERIAL_NUMBER === null) || (act.SERIAL_NUMBER === undefined)){
							act.CLASSIFICAZIONE_MATERIALE = 'CONSUMABILE';
						} else {
							act.CLASSIFICAZIONE_MATERIALE = 'NON CONSUMABILE';
						}
					
					});
					
					console.log(aFullResults);
					
					$scope.$ctrl.listaAttivitaMassive = _.sortBy(aFullResults,'IS_MASTER_SELECTED').reverse();
					
					$scope.$ctrl.mostraLoaderUiModal = false;
					
		
			});
		
		} /* fine TA_STAFFETTA */
		
		/* TA_DICHIARAZIONE_USO_MATERIALE */
		
		else if ($scope.$ctrl.azioneAttivita.tabella === 'TA_DICHIARAZIONE_USO_MATERIALE'){
			
			var filterGridTaDicUso = {};
			
			$scope.$ctrl.mostraLoaderUiModal = true;
			$scope.$ctrl.nascondiConferma = true;
			
			
			/* ripristino tutto */
			
			if ($scope.$ctrl.azioneAttivita.value === 'FORZA_RIENTRO_MAG_INTERVENTO') {
				$scope.$ctrl.buttonDicUsoCfg	= [
					{
						name			: 'NON_UTILIZZATO',
						label 		: 'Non Utilizzato',
						disabled	: true,
						icon			: 'fa fa-thumbs-down',
						value			: 'Non Utilizzato',
						tooltip		: 'Causale dichiarazione uso: Non Utilizzato',
						css				: 'btn-linkedin'
					}
				];	
			} else {
				$scope.$ctrl.buttonDicUsoCfg	= [
					{
						name			: 'INSTALLATO',
						label 		: 'Installato',
						disabled	: true,
						icon			: 'fa fa-thumbs-up',
						value			: 'Installato',
						tooltip		: 'Causale dichiarazione uso: Installato',
						css				: 'btn-dropbox'
					},
					{
						name			: 'NON_UTILIZZATO',
						label 		: 'Non Utilizzato',
						disabled	: true,
						icon			: 'fa fa-thumbs-down',
						value			: 'Non Utilizzato',
						tooltip		: 'Causale dichiarazione uso: Non Utilizzato',
						css				: 'btn-linkedin'
					},
					{
						name			: 'GUASTO',
						label 		: 'Guasto',
						disabled	: true,
						icon			: 'fa fa-wrench',
						value			: 'Guasto',
						tooltip		: 'Causale dichiarazione uso: Guasto',
						css				: 'btn-dropbox'
					},
					{
						name			: 'NON_CONFORME',
						label 		: 'Non Conforme',
						disabled	: true,
						icon			: 'fa fa-tag',
						value			: 'Non Conforme',
						tooltip		: 'Causale dichiarazione uso: Non Conforme',
						css				: 'btn-linkedin'
					}
			
				];		
			}				
			
			
			
			$scope.$ctrl.config.DICHIARAZIONE_USO.visibleCol = false;
			
			var aStatiSplit = $scope.$ctrl.azioneAttivita.filterTabella.STATO.split(',');
			
			console.log('aStatiSplit: ',aStatiSplit);
			
			if (aStatiSplit.length > 1) {
				/* solo per STATO e ID_ATTIVITA_MASTER */
				filterGridTaDicUso = {
					filter_STATO_in									: $scope.$ctrl.azioneAttivita.filterTabella.STATO,
					filter_ID_ATTIVITA_MASTER_equals				: $scope.$ctrl.item.ID_ATTIVITA
				};
			} else {
				/* solo per STATO e ID_ATTIVITA_MASTER */
				filterGridTaDicUso = {
					filter_STATO_equals								: $scope.$ctrl.azioneAttivita.filterTabella.STATO,
					filter_ID_ATTIVITA_MASTER_equals				: $scope.$ctrl.item.ID_ATTIVITA
				};				
			}
		
			/* 03/09/2021 - Richiesta 79 - DUSO da FILE - se GUI lancio la query dei materiali */
			if ($scope.$ctrl.checkDicUsoDaFile === 'GUI') {
		
				getTaDicUsoNoBufferGrid.getData(filterGridTaDicUso).$promise.then(function(result) {
						
						console.log(result.data.results);
					
						/* ciclo i valori trovati e ingetto il selectBox */
					
						angular.forEach(result.data.results, function(act) {
						
							console.log(act);
							if ($scope.$ctrl.item.GESTIONE_ATOMICA == 'SI') {
								act.SELECTED =  true;
								$scope.$ctrl.activitiesSelected.push(act);
							} else {
								act.SELECTED = false;	
							}
						});
						
						console.log(result.data.results);
					
						$scope.$ctrl.listaAttivitaMassive = _.sortBy(result.data.results,'SELECTED').reverse();
					
						$scope.$ctrl.mostraLoaderUiModal = false;
						
			
				});
			
			} else {
				console.log("MODALITA FILE - ESEGUO LA QUERY PER RIEMPIRE L'EXPORT EXCEL");
				
				$scope.$ctrl.listaAttivitaMassive = [];
				getTaDicUsoNoBufferGrid.getData(filterGridTaDicUso).$promise.then(function(result) {
						
						console.log(result.data.results);
					
						$scope.$ctrl.dataValueExportExcel	= _.sortBy(result.data.results,'ID_SIRTI');
						$scope.$ctrl.listaAttivitaMassive	= $scope.$ctrl.dataValueExportExcel;
					
						$scope.$ctrl.mostraLoaderUiModal = false;
						
			
				});

			}
		
		} /* fine TA_DICHIARAZIONE_USO_MATERIALE */
		
		
		/* TA_TRASFERIMENTO */
		
		else if ($scope.$ctrl.azioneAttivita.tabella === 'TA_TRASFERIMENTO'){
			
			var filterGridTaStaff = {};
			
			$scope.$ctrl.mostraLoaderUiModal = true;
			$scope.$ctrl.nascondiConferma = false;
			
			$scope.$ctrl.config.DICHIARAZIONE_USO.visibleCol = false;
			
			/* solo per NOT_IN_STATO e ID_ATTIVITA_MASTER */
			
			var aStatiSplit = $scope.$ctrl.azioneAttivita.filterTabella.STATO.split(',');
			
			console.log('aStatiSplit: ',aStatiSplit);
			
			if (aStatiSplit.length > 1) {
				/* solo per STATO e ID_ATTIVITA_MASTER */
				filterGridTaStaff = {
					filter_STATO_notin								: $scope.$ctrl.azioneAttivita.filterTabella.STATO,
					filter_ID_ATTIVITA_MASTER_equals				: $scope.$ctrl.item.ID_ATTIVITA
				};
			} else {
				/* solo per STATO e ID_ATTIVITA_MASTER */
				filterGridTaStaff = {
					filter_STATO_notequals							: $scope.$ctrl.azioneAttivita.filterTabella.STATO,
					filter_ID_ATTIVITA_MASTER_equals				: $scope.$ctrl.item.ID_ATTIVITA
				};				
			}
		
		
			getTaTrasferimentoNoBufferGrid.getData(filterGridTaStaff).$promise.then(function(result) {
					
					console.log(result.data.results);
				
					/* ciclo i valori trovati e ingetto il selectBox */
				
					angular.forEach(result.data.results, function(act) {
					
						console.log(act);
						if ($scope.$ctrl.item.GESTIONE_ATOMICA == 'SI') {
							act.SELECTED =  true;
							$scope.$ctrl.activitiesSelected.push(act);
						} else {
							act.SELECTED = false;	
						}
						
						if ((!act.SERIAL_NUMBER) || (act.SERIAL_NUMBER === '') || (act.SERIAL_NUMBER === null) || (act.SERIAL_NUMBER === undefined)){
							act.CLASSIFICAZIONE_MATERIALE = 'CONSUMABILE';
						} else {
							act.CLASSIFICAZIONE_MATERIALE = 'NON CONSUMABILE';
						}						
						
					});
					
					console.log(result.data.results);
				
					$scope.$ctrl.listaAttivitaMassive = _.sortBy(result.data.results,'SELECTED').reverse();
				
					$scope.$ctrl.mostraLoaderUiModal = false;
					
		
			});
		
		} /* fine TA_TRASFERIMENTO */		
		
	};

	

	$scope.$ctrl.hasError = {};
	
	
	
	$scope.$ctrl.myColSpan = {
		PART_NUMBER : 'col-md-12 col-xs12',
	};

	
	/* ripristina modale */

	
	$scope.$ctrl.ripristinaModale = function (){
	
		
		/* resetto la sezione verify con tutti i campi, in base all'azione chiamante */
		console.log('muori');
		
		$scope.$ctrl = angular.copy(oriCtrl);
		$scope.fModalAzione.$setPristine();
	
				
	};
	


  $scope.$ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };


	/* */
	
	$scope.$ctrl.clickConferma = function () {
		

		
		if($scope.$ctrl.options){
			$scope.$ctrl.options.global = '';
		}
		
		$scope.$ctrl.hasErrorAct 		= {};
		$scope.$ctrl.hasErrorEscAct 	= {};
		$scope.$ctrl.hasErrorIcon 		= {};
		$scope.$ctrl.hasSuccessEscAct	= {};
		
		$scope.$ctrl.mostraLoaderUiModal = true;
	
		console.log($scope.$ctrl.actionSelected);
		console.log($scope.$ctrl.chiamante.name);
		var datiCampilati =	{};
		var prosegui = false;	
		

	};
	

	/* paginazione dei risultati */
	
	$scope.$ctrl.currentPage  = 1;
	$scope.$ctrl.itemsPerPage = 10;
	$scope.$ctrl.orderByField = 'ID_ATTIVITA';
	$scope.$ctrl.reverseSort  = false;
	$scope.$ctrl.maxSizePager = 5;
	$scope.$ctrl.pager = [2, 5, 10, 20, 50,75,100];
	
	/* cambia pagina */ 
	$scope.$ctrl.pageChanged = function() {
		var startPos = ($scope.$ctrl.currentPage - 1) * $scope.$ctrl.itemsPerPage;
		return startPos;
	};
	
	/* gestione della scrollbar-y della grid */
	$scope.$watch('$ctrl.itemsPerPage', function(pg) {
		$timeout(function() {
			console.log('offsetWidth: ',angular.element('#tBodyGridModaleSped').prop('offsetWidth'));
			console.log('scrollWidth: ',angular.element('#tBodyGridModaleSped').get(0).scrollWidth);
			var	offsetWidth	=	angular.element('#tBodyGridModaleSped').prop('offsetWidth');
			var	scrollWidth	=	angular.element('#tBodyGridModaleSped').get(0).scrollWidth;
			if(scrollWidth === offsetWidth){
				angular.element('#tHeadGridModaleSped').css('min-width', offsetWidth);
				angular.element('#tHeadGridModaleSped').css('max-width', offsetWidth);
				angular.element('#tHeadGridModaleSped').css('width', offsetWidth);
			} else {
				angular.element('#tHeadGridModaleSped').css('min-width', scrollWidth);
				angular.element('#tHeadGridModaleSped').css('max-width', scrollWidth);
				angular.element('#tHeadGridModaleSped').css('width', scrollWidth);
			}
		});
	});	
	
	
	
// funzione di export tabella
$scope.$ctrl.exportToExcel = function(){
	
	
	
	$scope.$ctrl.mostraLoaderUiModal = true;
	
	var confHeader	= {};
	var dataValue		=	{};
	var nomeFile 		= '';
	var nomeSheet		= '';
	
	
		
	nomeSheet = 'ATTIVITA';
	
	nomeFile	= $scope.$ctrl.chiamante.name;
	
	angular.forEach($scope.$ctrl.config, function(value,key) {
		console.log(key);
		console.log(value);
		if ((key !=='SELECTED') && value.visibleInAction === false){
			confHeader[key] = {name : value.name, id : value.name, label : value.label, visible : true, enabled : false, type:'string' };
		}
	
	});
	
	dataValue = $scope.$ctrl.listaAttivitaMassive;
	
	// call services macrotasks


		var data = [];
		// faccio logica per definire il set dati da printare
		var dateNow = new Date();
		dateNow = $filter('date')(dateNow,'yyyyMMddHHmmss');
	
		var obj = {
				header	:	confHeader,
				data		:	dataValue
		};
		// ciclo su di esso
		obj = filterNumbers(filterDate(obj));
		console.log(obj);
		_.each(obj.data, function(row) {
			// costruzione nuovo oggetto per la stampa
			var printObj = {};
			_.each(row,function(val,key){
				// restituisce la row modificata con le sole chiavi con visibilita a true
				//console.log(confHeader[key]);
				
				if((confHeader[key]) && (confHeader[key].visible)){ printObj[confHeader[key].label] = val; }
			});
			// add all'array 
			data.push(printObj);
		});

		var ws =  XLSX.utils.json_to_sheet (data);
		// costruttore obj excel
		var wb =  XLSX.utils.book_new ();
		// appendo gli elementi al file
		XLSX.utils.book_append_sheet (wb, ws, nomeSheet );
		// scrivo il file
		XLSX.writeFile(wb, nomeFile+'_'+dateNow+'.xlsx');

		$scope.$ctrl.mostraLoaderUiModal = false;

};

	/* gestione mostra nascondi colonne grid */
	$scope.$ctrl.selectAllColonneGrid = function(option) {
		_.each($scope.$ctrl.config, function(value,key) {
			
			if (value.mostraNascondi && key !== 'FUNCTION'){
				
				value.visibleCol = option==='TUTTI' ? true:false;
			}
		});
	};
	
/* procedura che gestisce l'azione selezionata */
$scope.$ctrl.eseguiAzioneSelezionata = function(chiamante,objValue){
	console.log('eseguiAzioneSelezionata');
	console.log('chiamante: ',chiamante);
	console.log('objValue',objValue);
	
	
	$scope.$ctrl.actionSelected = {
		actionObj				: {},
		listaMateriali	: [],
		objInputValue 	: {}
	};
	
	//controllo se ci sono materiali selezionati
	
	if( $scope.$ctrl.activitiesSelected.length > 0 ){
		
		/* costruisco l'obj che servira' per eseguire l'azione in caso di accettazione */
		$scope.$ctrl.disableCbByActionSel = true;
		$scope.$ctrl.actionSelected.listaMateriali	= $scope.$ctrl.activitiesSelected;
		$scope.$ctrl.actionSelected.actionObj				= chiamante;
		$scope.$ctrl.actionSelected.objInputValue		= objValue;
		
		
	} else {
		
	}
	
};

/* procedura che conferma/annulla l'azione selezionata */
$scope.$ctrl.confermaAzioneSelezionata = function(name){
	if(name === 'SI'){ // confermo l'esecuzione dell'azione
		
		console.log($scope.$ctrl.actionSelected);
		

		if($scope.$ctrl.options){
			$scope.$ctrl.options.global = '';
		}		
		
		$scope.$ctrl.hasErrorAct 			= {};
		$scope.$ctrl.hasErrorEscAct 	= {};
		$scope.$ctrl.hasErrorIcon			= {};
		$scope.$ctrl.hasSuccessEscAct	= {};
		var paramInsertBuffer					= {};

		
		$scope.$ctrl.mostraLoaderUiModal = true;
		
		// gestisco i casi PRELEVATO e MATERIALE_RICEVUTO
		if (($scope.$ctrl.actionSelected.actionObj.value === 'PRELEVATO') || ($scope.$ctrl.actionSelected.actionObj.value === 'MATERIALE_RICEVUTO')) {
			
			
			/* cotruisco i parametri da inviare alla tabella di buffer */
			
			paramInsertBuffer = {
				LISTA_ATTIVITA	:	$scope.$ctrl.actionSelected.listaMateriali,
				PLSQL						:	$scope.$ctrl.actionSelected.actionObj.plsql,
				DICHIARAZIONE		:	''
			};
			
		} // gestisco i casi DICHIARAZIONE_USO_MATERIALE 
		else if ($scope.$ctrl.actionSelected.actionObj.value === 'DICHIARAZIONE_USO_MATERIALE') {
			
			
			/* cotruisco i parametri da inviare alla tabella di buffer */
			
			paramInsertBuffer = {
				LISTA_ATTIVITA	:	$scope.$ctrl.actionSelected.listaMateriali,
				PLSQL						:	$scope.$ctrl.actionSelected.actionObj.plsql,
				DICHIARAZIONE		:	$scope.$ctrl.actionSelected.objInputValue.DICHIARAZIONE_USO
			};

		} // gestisco i casi FORZA_RIENTRO_MAG_INTERVENTO 
		else if ($scope.$ctrl.actionSelected.actionObj.value === 'FORZA_RIENTRO_MAG_INTERVENTO') {
			
				
			/* gestisco la forzatura caso x caso */

			/* TA_ORDINE_SPEDIZIONE */
			if ($scope.$ctrl.actionSelected.actionObj.tabella === 'TA_ORDINE_SPEDIZIONE') { /* TA_ORDINE_SPEDIZIONE */
				paramInsertBuffer = null;
				
				
				
				
				paramInsertBuffer = {
					LISTA_ATTIVITA		:	$scope.$ctrl.actionSelected.listaMateriali,
					PLSQL				:	$scope.$ctrl.actionSelected.actionObj.plsql,
					DICHIARAZIONE		:	''
				};				
				
				
				
				//$scope.$ctrl.alert.warning('In progress -> Tabella: TA_ORDINE_SPEDIZIONE - Procedura: FORZA_RIENTRO_MAG_INTERVENTO - PlSql: ForzaRientroMagIntervOrdSped', { ttl: 10000 });
				
			} else if ($scope.$ctrl.actionSelected.actionObj.tabella === 'TA_STAFFETTA') { /* TA_STAFFETTA */
				paramInsertBuffer = null;
				
				
				
				
				paramInsertBuffer = {
					LISTA_ATTIVITA		:	$scope.$ctrl.actionSelected.listaMateriali,
					PLSQL				:	$scope.$ctrl.actionSelected.actionObj.plsql,
					DICHIARAZIONE		:	''
				};				
				
								
				
				//$scope.$ctrl.alert.warning('In progress -> Tabella: TA_STAFFETTA - Procedura: FORZA_RIENTRO_MAG_INTERVENTO - PlSql: ForzaRientroMagIntervOrdStaff', { ttl: 10000 });
				
			} else if ($scope.$ctrl.actionSelected.actionObj.tabella === 'TA_DICHIARAZIONE_USO_MATERIALE') { /* TA_DICHIARAZIONE_USO_MATERIALE */

			
				/* cotruisco i parametri da inviare alla tabella di buffer */
			
				paramInsertBuffer = {
					LISTA_ATTIVITA		:	$scope.$ctrl.actionSelected.listaMateriali,
					PLSQL				:	$scope.$ctrl.actionSelected.actionObj.plsql,
					DICHIARAZIONE		:	$scope.$ctrl.actionSelected.objInputValue.DICHIARAZIONE_USO
				};
				
				//$scope.$ctrl.alert.warning('In progress -> Tabella: TA_DICHIARAZIONE_USO_MATERIALE - Procedura: FORZA_RIENTRO_MAG_INTERVENTO - PlSql: ForzaRientroMagIntervDicUso', { ttl: 10000 });
				
			} else if ($scope.$ctrl.actionSelected.actionObj.tabella === 'TA_TRASFERIMENTO') { /* TA_TRASFERIMENTO */
				paramInsertBuffer = null;
				
				
				
				paramInsertBuffer = {
					LISTA_ATTIVITA		:	$scope.$ctrl.actionSelected.listaMateriali,
					PLSQL				:	$scope.$ctrl.actionSelected.actionObj.plsql,
					DICHIARAZIONE		:	''
				};				
				
							
				
				//$scope.$ctrl.alert.warning('In progress -> Tabella: TA_TRASFERIMENTO - Procedura: FORZA_RIENTRO_MAG_INTERVENTO - PlSql: ForzaRientroMagIntervTrasf', { ttl: 10000 });
			}
			
			

		} else {
			paramInsertBuffer = null;
		}
		
		
		if (paramInsertBuffer === null) {
			
			console.log('caso non gestito');
			
			$scope.$ctrl.alert.error('Azione non gestita', { ttl: 10000 });
			
			$scope.$ctrl.showVerifyMassiva = false;
			
			$scope.$ctrl.mostraLoaderUiModal = false;
			$scope.$ctrl.collapseSummary = false;
			$scope.$ctrl.showCbSelectedGrid = false;
			$scope.$ctrl.showInfoErrorGrid = true;
			$scope.$ctrl.showSelectAllGrid = false;
			
		} else {
		
			console.log('paramInsertBuffer: ',paramInsertBuffer);
			
			console.log($scope.$ctrl.actionSelected.actionObj.value+' -> azInsertMtzAppMobileReqBufferMassive');
			
			azInsertMtzAppMobileReqBufferMassive.getData(paramInsertBuffer).$promise.then(function(resultAz) {
			
				console.log(resultAz.data);
			
				if (resultAz.data.length > 0){
					var contaErr = 0;
					angular.forEach(resultAz.data, function(esitoAct) {
						console.log(esitoAct);
						//esitoAct.ESITO = 'KO'; /* fix me - da togliere */
						
						console.log('listaAttivitaMassive: ',$scope.$ctrl.listaAttivitaMassive);
						
						if (esitoAct.ESITO === 'KO'){
							contaErr++;
							_.each( _.where($scope.$ctrl.listaAttivitaMassive , { ID_ATTIVITA : esitoAct.ID_ATTIVITA } ) , function(att){
								console.log(att);
								$scope.$ctrl.hasErrorAct[att.ID_ATTIVITA]		= 'round round-xs hollow red';
								$scope.$ctrl.hasErrorIcon[att.ID_ATTIVITA]		= 'glyphicon-remove';
								$scope.$ctrl.hasErrorEscAct[att.ID_ATTIVITA]	= esitoAct.DESC_ESITO;
							});
						}
						if (esitoAct.ESITO === 'OK'){
							_.each( _.where($scope.$ctrl.listaAttivitaMassive , { ID_ATTIVITA : esitoAct.ID_ATTIVITA } ) , function(att){
								console.log(att);
								$scope.$ctrl.hasErrorAct[att.ID_ATTIVITA] = 'round round-xs hollow green';
								$scope.$ctrl.hasErrorIcon[att.ID_ATTIVITA]		= 'glyphicon-ok';
								$scope.$ctrl.hasSuccessEscAct[att.ID_ATTIVITA]	= esitoAct.DESC_ESITO;
							});
						}
						
					});
					if(contaErr > 0){
						$scope.$ctrl.alert.error('Errore movimentazione di tutte le attivit&agrave; selezionate. Vedere nel Riepilogo il dettaglio delle attivit&agrave; che hanno generato il problema', { ttl: 10000 });
									$scope.$ctrl.showVerifyMassiva = false;
			
									$scope.$ctrl.mostraLoaderUiModal = false;
									$scope.$ctrl.collapseSummary = false;
									$scope.$ctrl.showCbSelectedGrid = false;
									$scope.$ctrl.showInfoErrorGrid = true;
									$scope.$ctrl.showSelectAllGrid = false;
			
					}
					else{
						$scope.$ctrl.alert.success('Attivit&agrave; prese in carico dal sistema in attesa di essere processate.', { ttl: 10000 });
									$scope.$ctrl.showVerifyMassiva = false;
									$scope.$ctrl.mostraLoaderUiModal = false;
									$scope.$ctrl.collapseSummary = false;
									$scope.$ctrl.showCbSelectedGrid = false;
									$scope.$ctrl.showInfoErrorGrid = true;
									$scope.$ctrl.showSelectAllGrid = false;
			
					}
				}
				else{
					$scope.$ctrl.alert.error('Errore movimentazione di tutte le attivit&agrave;', { ttl: 10000 });
									$scope.$ctrl.showVerifyMassiva = false;
			
									$scope.$ctrl.mostraLoaderUiModal = false;
									$scope.$ctrl.collapseSummary = false;
									$scope.$ctrl.showCbSelectedGrid = false;
									$scope.$ctrl.showInfoErrorGrid = true;
									$scope.$ctrl.showSelectAllGrid = false;
			
				}
			});
		}

	} else { // se NO resetto tutto 
		$scope.$ctrl.disableCbByActionSel = false;
		$scope.$ctrl.actionSelected = {
			actionObj				: {},
			listaMateriali	: [],
			objInputValue 	: {}
		};
		
		if($scope.$ctrl.options){
			$scope.$ctrl.options.global = '';
		}		
		
		$scope.$ctrl.hasErrorAct 			= {};
		$scope.$ctrl.hasErrorEscAct 	= {};
		$scope.$ctrl.hasErrorIcon			= {};
		$scope.$ctrl.hasSuccessEscAct	= {};
		var paramInsertBuffer					= {};		

	}
};


	$timeout(function() {
		$scope.$ctrl.getGridMassiva();
	},400);

	// pulisco filtri
	$scope.$ctrl.clearFiltersAndOrdering = function() {
		if($scope.$ctrl.options){
			$scope.$ctrl.options.global = '';
		}
		// invoco il reload della tabella
		$scope.$ctrl.getGridMassiva();
	};
	
	
	$scope.$ctrl.viewActivity = function(item,key){
		// restituisce l'URL dinamico creato da
		var urlMappa =  function(){
			var urlPolaris = ActivityViewServices.getUrl({
				'stile':'custom_rma_sirti',//FIXME dedicare un classe lato polaris
				'id_attivita_passata':item[key]
			});
		
			return urlPolaris;
		};
		
		//$window.open(urlMappa,'_blank','location=no');
		var urlActivity = {src: urlMappa(), title:'ACTIVITY ID : '+item[key]+' '};
	
		/* UI MODALE - Inizio */
		var $ctrlVA = this;
		
		$ctrlVA.animationsEnabled = true;
		$ctrlVA.open = function (size, parentSelector,itemSelected,uiChiamante,uiConfig,tab,urlActivity) {
			/*var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;*/
			var modalInstance = $uibModal.open({
				animation: $ctrlVA.animationsEnabled,
				backdrop:false,
				keyboard:false,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: './views/common/activityViewModal.html',
				controller: 'ActivityViewModalCtrl',
				controllerAs: '$ctrl',
				size: size,
				//appendTo: parentElem,
				resolve: {
						itemSelected: function () { return itemSelected; },
						uiChiamante: function () { return uiChiamante; },
						uiConfig: function () { return uiConfig; },
						tab: function () { return tab; },
						givenUrl:function(){return urlActivity;}
				}
			});
			modalInstance.result.then(function (selectedItem) {
				$log.info('selectedItem:',selectedItem);
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
				
			});
		};
		$ctrlVA.open('fullscreen',null,item, $scope.$ctrl.chiamante ,$scope.$ctrl.config,'',urlActivity);
	};	

// jscs:enable requireCamelCaseOrUpperCaseIdentifiers	
}

angular.module('guiMtzNocMasterV2').controller('modalAzioneCtrl', modalAzioneCtrl);