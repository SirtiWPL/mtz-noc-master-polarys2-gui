'use strict';

/**
* @ngdoc overview
* @name guiMtzNocMasterV2
* @description # guiMtzNocMasterV2
*
* Main module of the application.
*/

								
angular.module('guiMtzNocMasterV2',
	[ 'ui.router', 'ui.bootstrap', 'ngAnimate','ngSanitize', 'uiServices', 'ngResource', 'satellizer', 'Utils',
		'angular-growl', 'ui.select', 'angular-timeline', 'gettext', 'Alerts', 'art.locale', 'version',
		'gsUiNocServices', 'advlogProfileServices', 'advlogwsServices', 
		'artApiServices','ngFileUpload','SirtiFileUploadDirective','monospaced.qrcode','io-barcode',
		'ncy-angular-breadcrumb','gsUiPmAndAssTecniciServices','ngTagsInput','SirtiColumnSortAndFilterDirective',
		'vsGoogleAutocomplete','gsCommonServices'])
							
.config(function($breadcrumbProvider) {
	$breadcrumbProvider.setOptions({
		//prefixStateName : 'home',
		// template: 'bootstrap3',
		templateUrl : 'views/common/breadcrumb.html'
	});
})
							
.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/home');

	$stateProvider
		.state('notAuthorized', {
			url: '/not-authorized',
			templateUrl: 'views/common/not-authorized.html'
		})
		.state('index', {
			url: '/home',
			templateUrl: 'views/main.html',
			controller:'guiMtzNocMasterV2Ctrl',
			ncyBreadcrumb : {
				icon : 'glyphicon glyphicon-home',
				label :'Home',
				disabled:false
			}
		})
		
		
		//UI_NOC
		.state('uiNoc', {
			url: '/uiNoc',
			templateUrl: 'views/uiNoc/gsUiNoc.html',
			abstract:false,
			params: { section: 'UI_NOC' },
			ncyBreadcrumb : {
				icon : 'fa fa-dashboard',
				label :'Gestione Noc',
				disabled:false,
				visibled:true,
				parent:'index'
			}
		})
		//UI_PM_E_ASS_TECNICO
		.state('uiPmAndAssTecnici', {
			url: '/uiPmAndAssTecnici',
			//templateUrl: 'views/uiPmAndAssTecnici/gsUiPmAndAssTecnici.html',
			templateUrl: 'views/uiPmAndAssTecnici/gsUiPmAndAssTecnici.html',
			abstract:false,
			params: { section: 'UI_PM_E_ASS_TECNICI' },
			ncyBreadcrumb : {
				icon : 'fa fa-users',
				label :'Gestione PM e Ass. Tecnici',
				disabled:false,
				visibled:true,
				parent:'index'
			}
		})
		//UI_NUOVO_INTERVENTO
		.state('uiNuovoIntevento', {
			url: '/uiNuovoIntevento',
			//templateUrl: 'views/uiPmAndAssTecnici/gsUiPmAndAssTecnici.html',
			templateUrl: 'views/uiNuovoIntevento/uiNuovoIntevento.html',
			abstract:false,
			params: { section: 'UI_NUOVO_INTERVENTO' },
			ncyBreadcrumb : {
				icon : 'fa fa-edit',
				label :'Gui Nuovo Intervento',
				disabled:false,
				visibled:true,
				parent:'index'
			}
		})
		;
	})

	.config(function ($authProvider) {
		$authProvider.authHeader = 'Authorization';
		$authProvider.authToken = 'JWT';
		$authProvider.withCredentials = false;
		/*
		 * sessionStorage will only be accessible while and by the window that created it is open.
		 * localStorage lasts until you delete it or the user deletes it.
		 */
		$authProvider.storageType = 'sessionStorage';
	})
				
	.run(function ($rootScope, $auth, $locale , $window, $q, ConfigService, ProfileService, ProfileConfig, ADVLOGWSConfigService, 
		 ADVLOGWSConfig, Alert, gettextCatalog,$timeout, _) {
		// run gettextCatalog
		gettextCatalog.setCurrentLanguage($locale.id);
		gettextCatalog.debug = false;
		
		$rootScope.canGoOn = false;
		$rootScope.isAppKO = false;

		
		ConfigService.get(function(data) {
			
			$rootScope.config = data;
			$auth.setToken(data.jwtToken);
			// invoco i servizi remoti e ne estraggo le promise restituite
			(new ProfileService()).get(
				function(data) {
					
					ProfileConfig.set(data);
					
					function getConfigPromise(ConfigService) {
						var deferred = $q.defer();
						
						(new ConfigService()).get(
							function(data) {
								deferred.resolve({ config: data, err: null });
							},
							function(err) {
								deferred.resolve({ config: null, err: err });
							}
						);
						
						return deferred.promise;
					}
					
					$q.all([
						getConfigPromise(ADVLOGWSConfigService),
						/*getConfigPromise(AdcConfigService)*/
					]).then(function(data) {
						// le promise sono state tutte risolte, in data ho l'array dei valori restituiti
						// dai servizi remoti, nell'ordine indicato in $q.all
						// nota che queste promise vengono sempre risolte anche se la chiamata ajax non va a buon fine!
						_.each([ ADVLOGWSConfig], function(service, index) {
							service.set(data[index].config, data[index].err);
						});
						$rootScope.showBreadcrumb = true;
						$rootScope.canGoOn = true;
					});
				},
				function(err) {
					if (err.data){
						Alert.fatal(err.data);
					} else {
						Alert.fatal(gettextCatalog.getString('Unknown error (HTTP status: {{errStatus}})', { errStatus: err.status }));
						$timeout( function(){ window.location.href = ''+$rootScope.config.AdvlogHomePage+''; }, 400 );
					}
					$rootScope.isAppKO = true;
				}
			);
		});
		
	})
;
