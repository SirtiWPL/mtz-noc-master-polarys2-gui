'use strict';

angular.module('artApiServices', [])

.factory('SessionService', function($resource, $rootScope) {
	return $resource($rootScope.config.GestMagApiService + 'sessions/:TOKEN',{},
		{
			login: {
				method: 'POST',
				params: {},
				withCredentials: true
			},
			logout: {
				method: 'DELETE',
				params: {},
				withCredentials: true
			}
		},
		{
			stripTrailingSlashes: true
		}
	);
})

.factory('ActivityService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.GestMagApiService + 'activities',{},
		{
			find: {
				method: 'GET',
				params: {
					excludeProperties: 0,
					showOnlyWithVisibility: 1
				},
				//isArray: true,
				withCredentials: true,
				interceptor: CheckAuthAndRefreshSessionInterceptor
			},
			findArray: {
				method: 'GET',
				params: {
					excludeProperties: 1,
					showOnlyWithVisibility: 1
				},
				isArray: true,
				withCredentials: true,
				interceptor: CheckAuthAndRefreshSessionInterceptor
			}
		},
		{
			stripTrailingSlashes: true
		}
	);
})

.factory('ActivityGetService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.GestMagApiService + 'activities/:ID',{},
			{
				get: {
					method: 'GET',
					params: {},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})

.factory('ActivityHistoryService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.GestMagApiService + 'activities/:ID/history',{},
			{
				get: {
					method: 'GET',
					params: {},
					isArray: true,
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				},
				step: {
					method: 'POST',
					params: {},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})

.factory('ActivityHierarchyService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.GestMagApiService + 'activities/:ID/hierarchy',{},
			{
				get: {
					method: 'GET',
					params: {},
					//isArray: true,
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})

.factory('ActivityLockService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.GestMagApiService + 'activities/:ID/lock',{},
			{
				lock: {
					method: 'PUT',
					params: {},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				},
				unlock: {
					method: 'DELETE',
					params: {},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})

.factory('ActivityUpdateService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.GestMagApiService + 'activities/:ID/activityProperties',{},
			{
				get: {
					method: 'GET',
					params: {},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor,
					isArray: true
				},
				update: {
					method: 'PUT',
					params: {},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})


/**
 * Restituisce la PERMISSION_ACTION  dei flussi ART
 */
.factory('ActivityFlowService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.GestMagApiService + 'instance/types/activities/:TYPE/:WHAT',{},
			{
				getFlow: {	// deve essere invocato passando TYPE (tipo attivitÃ )
					method: 'GET',
					params: {						
					},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				},
				enumActivityType: {	// deve essere invocato senza TYPE
					method: 'GET',
					params: {},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor,
					isArray: true
				},
				enumActivityTypeActions: {
					method: 'GET',
					params: {
						WHAT: 'actions'
					},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor,
					isArray: false
				},
				enumActivityTypeStatuses: {
					method: 'GET',
					params: {
						WHAT: 'statuses'
					},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor,
					isArray: false
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})


.factory('ActivityTypesService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.GestMagApiService + 'instance/types/activities',{},
			{
				get: {
					method: 'GET',
					params: {},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor,
					isArray: true
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})

.factory('ActivityPropertyTypesService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {

	return $resource($rootScope.config.GestMagApiService + 'instance/types/activities/:TYPE/properties',{},
			{
				get: {
					method: 'GET',
					params: {},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})

.factory('ActivityDestUsersService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {

	return $resource($rootScope.config.GestMagApiService + 'activities/:ID/destUsers/:ACTION',{},
			{
				get: {
					method: 'GET',
					params: {},
					isArray: true,
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})


.factory('tempFileUploadRoute', function($rootScope) {
	return function() { 
		return $rootScope.config.AdvlogwsAppService + 'api/art/tmpfiles';
	};
})

.factory('TempFileUpload', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.AdvlogwsAppService + 'api/art/tmpfiles/:ID',{},
		{
			remove: {
				method: 'DELETE',
				params: {},
				withCredentials: true,
				interceptor: CheckAuthAndRefreshSessionInterceptor
			}
		},
		{
			stripTrailingSlashes: true
		}
	);
})


/**
 * DEPRECATED!
 */
.factory('DownloadAttachmentRoute', function($rootScope) {
	return function(id, transitionId, sequence) { 
		return $rootScope.config.GestMagApiService + 'activities/' + id + '/history/' + transitionId + '/attachments/' + sequence;
	};
})

.factory('CanDoAction', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.GestMagApiService + 'activities/:ID/canDoAction/:ACTION',{},
			{
				get: {
					method: 'GET',
					params: {},
					isArray: true,
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})


/**
 * Fornisce l'elenco di tutti gli allegati di un'attività
 */
.factory('ActivityAttachmentsService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.GestMagApiService + 'activities/:ID/attachments',{},
			{
				get: {
					method: 'GET',
					params: {},
					isArray: true,
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})


/**
 * Consente di scaricare un allegato
 */
.factory('ActivityAttachmentService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor, getFilenameFromHeader) {
	return $resource($rootScope.config.GestMagApiService + 'activities/:ID/history/:TRANSITION_ID/attachments/:SEQUENCE',{},
			{
				download: {
					method: 'GET',
					responseType: 'blob',
					params: {},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor,
					// Trasformo la response per iniettare il nome del file dagli header
					transformResponse: function(data, headers) {
						return {
							data: data,
							filename: getFilenameFromHeader(headers)
						};
					}					
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})


.factory('SystemsService', function($resource, $rootScope, CheckAuthAndRefreshSessionInterceptor) {
	return $resource($rootScope.config.GestMagApiService + 'systems/:ID',{},
			{
				suspend: {
					method: 'PUT',
					params: {
					},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor,
					isArray: false
				},
				end: {
					method: 'DELETE',
					params: {},
					withCredentials: true,
					interceptor: CheckAuthAndRefreshSessionInterceptor,
					isArray: false
				}
			},
			{
				stripTrailingSlashes: true
			}
	);
})
;
