'use strict';

angular
	.module('gsCommonServices', [])
	.factory('printTableMock', function($resource) {
		return $resource('./mock-data/fakedata.json',{ }, {
			getData: {method:'GET', isArray: false}
		});
	})
	/*  POLARYS 2.0 - TA_ORDINE_SPEDIZIONE  */
	.factory('getTaOrdineSpedizioneGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/common/taOrdineSpedizione/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	/*  POLARYS 2.0 - TA_STAFFETTA  */
	.factory('getTaStaffettaGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/common/taStaffetta/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	/*  POLARYS 2.0 - TA_DICHIARAZIONE_USO_MATERIALE  */
	.factory('getTaDicUsoGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/common/taDicUso/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	/*  POLARYS 2.0 - V_POL2_GRUPPI_INTERV_GRID  */
	.factory('getGruppiInterventoPolarys', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/common/gruppiInterventoPolarys/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	/*  POLARYS 2.0 - V_POL2_TA_ORD_SPED_NOBUFF_GRID  */
	.factory('getTaOrdineSpedizioneNoBufferGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taOrdineSpedizioneNoBuffer/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	/*  POLARYS 2.0 - V_POL2_TA_ORD_SPED_INFO_GRID  */
	.factory('getTaOrdineSpedizioneInfoGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taOrdineSpedizioneInfo/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })	
	/*  POLARYS 2.0 - V_POL2_TA_ORD_SPED_CORR_GRID  */
	.factory('getTaOrdineSpedizioneCorriereGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taOrdineSpedizioneCorriere/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })	
	/*  POLARYS 2.0 - V_POL2_TA_STAFF_NOBUFF_GRID  */
	.factory('getTaStaffettaNoBufferGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taStaffettaNoBuffer/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	/*  POLARYS 2.0 - V_POL2_TA_STAFF_INFO_GRID  */
	.factory('getTaStaffettaInfoGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taStaffettaInfo/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	/*  POLARYS 2.0 - V_POL2_TA_STAFF_CORR_GRID  */
	.factory('getTaStaffettaCorriereGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taStaffettaCorriere/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })	
	/*  POLARYS 2.0 - V_POL2_TA_DIC_USO_NOBUFF_GRID  */
	.factory('getTaDicUsoNoBufferGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taDicUsoNoBuffer/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	/*  POLARYS 2.0 - V_POL2_TA_DIC_USO_INFO_GRID  */
	.factory('getTaDicUsoInforGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taDicUsoInfo/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	/*  POLARYS 2.0 - V_POL2_TA_ORD_SPED_CHECK_GRID  */
	.factory('getTaOrdSpedCheckGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taOrdSpedCheck/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	/*  POLARYS 2.0 - V_POL2_TA_STAFF_CHECK_GRID  */
	.factory('getTaStaffCheckGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taStaffCheck/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	/*  POLARYS 2.0 - V_POL2_TA_DIC_USO_CHECK_GRID  */
	.factory('getTaDicUsoCheckGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taDicUsoCheck/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	/*  POLARYS 2.0 - V_POL2_TA_TRASF_NOBUFF_GRID  */
	.factory('getTaTrasferimentoNoBufferGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taTrasferimentoNoBuffer/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	/*  POLARYS 2.0 - V_POL2_TA_TRASF_INFO_GRID  */
	.factory('getTaTrasferimentoInfoGrid', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taTrasferimentoInfo/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	
	/*  POLARYS 2.0 - V_POL2_TA_TRASF_CHECK_GRID  - add 06/10/2020 - nuova gestione dei Trasferimenti */
	.factory('getTaTrasferimentoCheck', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/taTrasferimentoCheck/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })	
	
	.factory('azInsertMtzAppMobileReqBuffer', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/common/pkgManutenzioni/action/insertMtzAppMobileReqBuffer', {}, {
				getData : {
						method : 'POST',
						params : {rollback:UiConfig.rollBack()},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	.factory('azInsertMtzAppMobileReqBufferMassive', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/common/pkgManutenzioni/action/insertMtzAppMobileReqBufferMassive', {}, {
				getData : {
						method : 'POST',
						params : {rollback:UiConfig.rollBack()},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
;
	


	


    