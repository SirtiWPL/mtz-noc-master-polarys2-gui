'use strict';

angular
.module('gsUiNocServices', [])
.factory('printTableMock', function($resource) {
	return $resource('./mock-data/fakedata.json',{ }, {
		getData: {method:'GET', isArray: false}
	});
})
/* S&R WIND: servizio che ritorna la lista delle attivita che popola la griglia dell UI */
.factory('getGridSwapRipairWindServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/swapRepair/wind/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})

/* RESO DA CAMPO: servizio che ritorna la lista delle attivita che popola la griglia dell UI */
.factory('getGridResoDaCampoPolarysServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/resi/polarys/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})

/* RIENTRO RIPARAZIONE: servizio che ritorna la lista delle attivita che popola la griglia dell UI */
.factory('getGridRientroRiparazioneServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/rientroRiparazione/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})

/* ADC: servizio che ritorna la lista delle attivita che popola la griglia dell UI */
.factory('getGridAdcPolarysServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/adc/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})	
/* RITIRI WIND - tab RESI : servizio che ritorna la lista delle attivita che popola la griglia dell UI */
.factory('getGridRitiriWindTabResoServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/resi/wind/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})	
/* RITIRI WIND - tab TRASFERIMENTI : servizio che ritorna la lista delle attivita che popola la griglia dell UI */
.factory('getGridRitiriWindTabTrasferimentiServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/trasferimenti/wind/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
/* TRASFERIMENTO_S2S : servizio che ritorna la lista delle attivita che popola la griglia dell UI */
.factory('getGridTrasferimentiS2SServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/trasferimento/sirti/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
/* TA_TRASFERIMENTO : per gestire le EM Trasferimento Massive su base ID_POLARYS */
.factory('getGridTaTrasferimentoServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/trasferimento/taTrasferimento/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
/* PYR_EM_MASSIVA : per gestire le EM Trasferimento Massive su base ID_POLARYS */
.factory('getGridPyrEmMassivaServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/trasferimento/pyrEmMassiva/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
/* STORICO DDT : servizio che ritorna la lista delle attivita per le quali è stato generato un ddt */
.factory('getGridDdtServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/listaDdt/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})

.factory('emStatisticheMenu', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerce/statisticheMenu', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})	

.factory('entrataMerciPyramideStd', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci', {}, {
		getData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('entrataMerciTrasferimentoSirti', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/emTrasferimentoSirti', {}, {
		getData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('entrataMerciTrasferimentoMassivoSirti', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/emTrasferimentoMassivoSirti', {}, {
			getData : {
					method : 'POST',
					params : {rollback:UiConfig.rollBack()},
					withCredentials : true,
					interceptor : CheckAuthAndRefreshSessionInterceptor
			}
	},{
			stripTrailingSlashes : true
	});
})
.factory('entrataMerciADC', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerciADC', {}, {
		getData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('entrataMerciResoWind', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/EMresoWind', {}, {
		getData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('entrataMerciTrasferimentoWind', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/EMTrasferimentoWind', {}, {
		getData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})

.factory('getGeneraNumeroDDT', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/generaNumeroDDT', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('getCheckRuoloSquadra', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/checkRole', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})	

/* procedura che ritorna la data min e max del campo custom DATA_RICEZIONE_MATERIALE della UI S&R */
.factory('getMinMaxDateSR', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/swapRepair/minMaxDate', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})			
/* procedura che valida la DATA_RICEZIONE_MATERIALE della UI S&R */
.factory('checkMinMaxDateSR', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/swapRepair/checkMinMaxDate', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
/* procedura che valida la fattibilita dell'EM nella UI - RESO DA CAMPO */
.factory('checkResoDaCampoPolarys', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/EMcheckResoPolarys', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('azRmaSirtiCambiaMagazzinoDestRientroDaRip', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/rientroDaRiparazione/RmaSirtiCambiaMagazzinoDest', {}, {
		getData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('azSmarritoResoDaCampo', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/resoDaCampo/Smarrito', {}, {
		getData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('azSospensioneRitiriWindTabReso', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/ritiriWind/reso/Sospensione', {}, {
		getData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('azAnnullamentoResoResoDaCampo', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/resoDaCampo/annullamentoReso', {}, {
		getData : {
			method : 'POST',
			params : {rollback:UiConfig.rollBack()},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('getGridAnagraficaSapServices', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/entrataMerci/anagraficaSap/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
.factory('getMotoreRicercaMaterialeDettaglio', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
	return $resource($rootScope.config.AdvlogwsAppService + 'pyr/motoreRicercaMateriale/materialiDettaglio/grid', {}, {
		getData : {
			method : 'GET',
			params : {},
			withCredentials : true,
			interceptor : CheckAuthAndRefreshSessionInterceptor
		}
	},{
		stripTrailingSlashes : true
	});
})
;
