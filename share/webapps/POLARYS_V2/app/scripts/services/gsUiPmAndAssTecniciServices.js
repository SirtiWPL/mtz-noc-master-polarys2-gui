'use strict';

angular
	.module('gsUiPmAndAssTecniciServices', [])
	.factory('printTableMock', function($resource) {
		return $resource('./mock-data/fakedata.json',{ }, {
			getData: {method:'GET', isArray: false}
		});
	})
	/*  POLARYS 2.0 - UI PM E ASS TECNICI - GRID  */
	.factory('getListaAttivitaUiPmAndAssTecnici', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/listaAttivita/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	.factory('azOrdineSpedizioneCambioModalitaSpedizione', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope,UiConfig) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/gestioneSpedizione/action/ordineSpedizione/CambioModalitaSpedizione', {}, {
				getData : {
						method : 'POST',
						params : {rollback:UiConfig.rollBack()},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
	/*  add 26/11/2020 - Nuova gestione della sezione Filtri - GRID - V_MTZ_POL2_SN_VS_ID_POLARYS */
	.factory('getEditFilterSnVsIdPolarys', function($resource, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
		return $resource($rootScope.config.AdvlogwsAppService + 'pyr/pyrMtzNocMaster/uiPmAndAssTecnici/getSnVsIdPolarys/grid', {}, {
				getData : {
						method : 'GET',
						params : {},
						withCredentials : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
				}
		},{
				stripTrailingSlashes : true
		});
  })
;
	


	


    