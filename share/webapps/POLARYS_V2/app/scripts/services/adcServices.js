'use strict';

/**
 * @ngdoc service
 * @name advlog.adcServices
 * @description # adcServices Factory in the advlog.
 */

angular.module('adcServices', [ 'Utils'])

	.value('AdcCanAccessObj', {})

	.run(function($rootScope, $q, AdcCanAccessObj) {
		$rootScope.Adc = {
			profilo: {
				gruppi: [],
				isProjectManager: false,
				isCapoMagazziniere: false,
				isMagazziniere: false,
				progetti: [],
				magazzini: []
			},
			tipiDistinta: {},
			causaliNonConformita: [],
			idrPrefix: '',
			err: undefined
		};
		AdcCanAccessObj.promise = $q.defer();
	})

	.factory('AdcConfigService', function($resource, _, CheckAuthInterceptor, $rootScope) {
		return function() {
			return $resource($rootScope.config.AdcAppService + 'config', {}, {
				get : {
					method : 'GET',
					params : {},
					withCredentials : true,
					interceptor : CheckAuthInterceptor
				}
			}, {
				stripTrailingSlashes : true
			});
		};
	})

	.factory('AdcConfig', function($rootScope, AdcCanAccessObj) {
		return {
			set: function(config, err) {
				if(err) {
					AdcCanAccessObj.promise.reject('Not Authorized');
					$rootScope.Adc.err = err;
					return;
				}
				$rootScope.Adc.profilo.gruppi             = config.profilo.gruppi;
				$rootScope.Adc.profilo.isProjectManager   = config.profilo.isProjectManager;
				$rootScope.Adc.profilo.isCapoMagazziniere = config.profilo.isCapoMagazziniere;
				$rootScope.Adc.profilo.isMagazziniere     = config.profilo.isMagazziniere;
				$rootScope.Adc.profilo.progetti           = config.profilo.progetti;
				$rootScope.Adc.profilo.magazzini          = config.profilo.magazzini;

				$rootScope.Adc.tipiDistinta         = config.tipiDistinta;
				$rootScope.Adc.causaliNonConformita = config.causaliNonConformita;
				$rootScope.Adc.idrPrefix            = config.idrPrefix;
						
				AdcCanAccessObj.promise.resolve(true);
			}
		};
	})

	.factory('AdcProfile', function($rootScope, _, AdcCanAccessObj) {
		return {
			canAccess: function() {
				return AdcCanAccessObj.promise.promise;
			},
			gruppi: function() { return $rootScope.Adc.profilo.gruppi; },
			isProjectManager: function() { return $rootScope.Adc.profilo.isProjectManager; },
			isCapoMagazziniere: function() { return $rootScope.Adc.profilo.isCapoMagazziniere; },
			isMagazziniere: function() { return $rootScope.Adc.profilo.isMagazziniere; },
			isAdmin : function() { var ret = false; if($rootScope.userProfile.nomeUtente === 'ROOT'){ret = true;  return ret;} return ret;},
			progetti: function() { return $rootScope.Adc.profilo.progetti; },
			magazzini: function() { return $rootScope.Adc.profilo.magazzini; },
			/**
			 * Verifica se un magazzino sirti è tra i magazzini di competenza dell'utente
			 * @param {string} codMagSirti - codice magazzino sirti (idMagSirti + provincia, es 2107CT)
			 * @returns {Boolean} - true se il codice magazzino sirti passato in inupt è tra i magazzini
			 *                      di competenza, false altrimenti
			 */
			isMagazzinoiDiCompetenza: function(codMagSirti) {
				return !_.isUndefined(_.findWhere($rootScope.Adc.profilo.magazzini, { codMagSirti: codMagSirti }));
			},
			hasUniqueRole: function() {
				var ret = true;
				var PM = $rootScope.Adc.profilo.isProjectManager;
				var RM = $rootScope.Adc.profilo.isCapoMagazziniere;
				var MA = $rootScope.Adc.profilo.isMagazziniere;
				// valuto se sono in multiruolo
				if((PM && (RM || MA)) || (RM && (PM || MA)) || (MA && (PM || RM))) { ret = false; }
				return ret;
			},
			err: function() { return $rootScope.Adc.err; }
		};
	})

	.provider('adc', function() {
		this.modulo = 'adc';
		this.$get = function(gettextCatalog, AdcProfile){
			var modulo = this.modulo;
			var navItems = this.navItems;
			var userIcon = this.userIcon;
			return { 
				'getNavItems':function(){ 
					return navItems(modulo,gettextCatalog,AdcProfile);
				},
				'getModulo' : function(){
					return modulo;
				},
				'getUserIcon' : function(){
					return userIcon(gettextCatalog,AdcProfile);
				}
			};
		};
		
	})

	.factory('IdrPrefix', function($rootScope) {
		return {
			get: function() { 
				return $rootScope.Adc.idrPrefix; 
			}
		};
	})

	.factory('CausaliNonConformitaService', function($resource, _, CheckAuthInterceptor, $rootScope) {
		return function() {
			return $resource($rootScope.config.AdcAppService + 'causaliNonConformita', {}, {
				get : {
					method : 'GET',
					params : {},
					isArray: true,
					withCredentials : true,
					interceptor : CheckAuthInterceptor
				}
			}, {
				stripTrailingSlashes : true
			});
		};
	})

	.factory('CausaliNonConformita', function($rootScope) {
		return {
			lista: function() { 
				return $rootScope.Adc.causaliNonConformita; 
			}
		};
	})

	.factory('TipiDistintaService', function($resource, _, CheckAuthInterceptor, $rootScope) {
		return function() {
			return $resource($rootScope.config.AdcAppService + 'tipiDistinta', {}, {
				get : {
					method : 'GET',
					params : {},
					isArray: true,
					withCredentials : true,
					interceptor : CheckAuthInterceptor
				}
			}, {
				stripTrailingSlashes : true
			});
		};
	})

	.factory('TipiDistinta', function($rootScope, _) {
		return {
			tDist: function() { return $rootScope.Adc.tipiDistinta; },
			findById: function(id) { return _.findWhere($rootScope.Adc.tipiDistinta, { idTipoDistinta: id }); }
		};
	})

	// chiamata Rest per I Magazzini
	.factory('magazzini', function($resource, _, CheckAuthAndRefreshSessionInterceptor, $rootScope) {
				return $resource($rootScope.config.AdcAppService + 'magazzini/:id', {}, {
					codMag : {
						method : 'GET',
						params : {},
						isArray : true,
						interceptor : CheckAuthAndRefreshSessionInterceptor
					},
					findMagById : {
						method : 'GET',
						params : {},
						interceptor : CheckAuthAndRefreshSessionInterceptor
					}
				}, {
					stripTrailingSlashes : true
				});
			})

	.factory(
			'catalogo',
			function($resource, _,
					CheckAuthAndRefreshSessionInterceptor, $rootScope) {
				return $resource(
						$rootScope.config.AdcAppService + 'catalogo/', {}, {
							getCatalogo : {
								method : 'GET',
								params : {},
								isArray : true,
								interceptor : CheckAuthAndRefreshSessionInterceptor
							}
						}, {
							stripTrailingSlashes : true
						});
			})

	
	// service get Event
	.service('adcWsService',['$log','$q','$http','$interval','$parse','distinteResurces',
		function($log, $q, $http, $interval, $parse,distinteResurces) {
			function asyncFind(config) {
				var deferred = $q.defer();
				setTimeout(function() {
					deferred.notify('About to greet');
					if (deferred) {
						deferred.resolve(distinteResurces.find(config));
					} else {
						deferred.reject('Greetingis not allowed.');
					}
				}, 1000);
				return deferred.promise;
			}
			return {
				asyncFind : asyncFind
			};
	} ]);
