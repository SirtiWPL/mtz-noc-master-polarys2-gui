'use strict';

var semver = require('semver');
var exec = require('child_process').execSync;

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Automatically load required Grunt tasks
  require('jit-grunt')(grunt, {
    exec: 'grunt-exec',
    version: 'grunt-version',
    prompt: 'grunt-prompt'
  });

  var pkg = grunt.file.readJSON('package.json');

  var versionUpgradeChoices = [
    {
      name: 'Prerelease (' + semver.inc(pkg.version, 'prerelease') + ')',
      value: 'prerelease'
    },
    {
      name: 'Prepatch (' + semver.inc(pkg.version, 'prepatch') + ')',
      value: 'prepatch'
    },
    {
      name: 'Patch (' + semver.inc(pkg.version, 'patch') + ')',
      value: 'patch'
    },
    {
      name: 'Preminor (' + semver.inc(pkg.version, 'preminor') + ')',
      value: 'preminor'
    },
    {
      name: 'Minor (' + semver.inc(pkg.version, 'minor') + ')',
      value: 'minor'
    },
    {
      name: 'Premajor (' + semver.inc(pkg.version, 'premajor') + ')',
      value: 'premajor'
    },
    {
      name: 'Major (' + semver.inc(pkg.version, 'major') + ')',
      value: 'major'
    },
    {
      name: 'Custom ...',
      value: 'custom'
    }
  ];

  // Define the configuration for all the tasks
  grunt.initConfig({

    // version upgrade
    version: {
      upgradeVersionPackageJson: {
        options: {
          release: 'prerelease'
        },
        src: ['package.json','share/webapps/GESTMAG/package.json']
      }
    },

    // build on "our own way"
    prompt: {
      continueDespiteWTDirty: {
        options: {
          questions: [
            {
              config: 'echo.continueDespiteWTDirty',
              type: 'confirm',
              default: false,
              message: 'Your Working Tree is dirty: do you wanna continue?'
            }
          ],
          then: function() {
            if(!grunt.config('echo.continueDespiteWTDirty')) {
              grunt.fail.fatal('User won\'t continue. Nothing will be done!');
            }
          }
        }
      },
      configMakeTagAndVersionUpgrade: {
        options: {
          questions: [
            {
              config: 'version.upgradeVersionPackageJson.options.release',
              type: 'list',
              message: 'Version upgrade type',
              default: 'prerelease',
              choices: versionUpgradeChoices
            },
            {
              config: 'echo.customVersion',
              type: 'input',
              message: 'Please insert version',
              default: '',
              when: function(answers) {
                return answers['version.upgradeVersionPackageJson.options.release'] === 'custom';
              },
              validate: function(value) {
                return semver.valid(value) ? true : 'Please enter a valid version. See ' + 'http://semver.org/'.blue.underline + ' for more details.';
              }
            },
            {
              config: 'echo.make_current_tag_message',
              type: 'input',
              message: 'Please insert tag message',
              default: '',
              validate: function(value) {
                if(value === '') { return 'Please enter a valid message'; }
                return true;
              }
            },
            {
              config: 'echo.skipGESTMAGBuild',
              type: 'confirm',
              default: false,
              message: 'Skip GESTMAG build creation?'
            },
            {
              config: 'echo.continue',
              type: 'confirm',
              message: function(answers) {
                var newVersion; 
                if(answers['version.upgradeVersionPackageJson.options.release'] === 'custom') {
                  newVersion = answers['echo.customVersion'];
                } else {
                  newVersion = semver.inc(pkg.version, answers['version.upgradeVersionPackageJson.options.release']);
                }
                var msg = ''
                if(!answers['echo.skipGESTMAGBuild']) {
                  msg += ' and GESTMAG build will be created';
                } else {
                  msg += ' and GESTMAG build WON\'T be created';
                }
                return 'Version will be upgraded to ' + newVersion + msg + '\n' + 'Are you sure?';
              }
            }
          ],
          then: function() {
            if(!grunt.config('echo.continue')) {
              grunt.fail.fatal('User won\'t continue. Nothing will be done!');
            }
            if(grunt.config('version.upgradeVersionPackageJson.options.release') === 'custom') {
              grunt.config('version.upgradeVersionPackageJson.options.release', grunt.config('echo.customVersion'));
            } else {
              grunt.config('version.upgradeVersionPackageJson.options.release', semver.inc(pkg.version, grunt.config('version.upgradeVersionPackageJson.options.release')));
            }
          }
        }
      }
    },

    // utility for build on "our own way"
    exec: {
      gruntBuildPlusPlusGESTMAG: {
        cmd: function() {
          if(grunt.config('echo.skipGESTMAGBuild')) {
            return 'echo "Nothing to do"';
          }
          return 'grunt build-plus-plus';
        },
        cwd: 'share/webapps/GESTMAG'
      },
      gitStatus: {
        cmd: function() {
          return 'git status';
        }
      },
      gitRmOldTags: {
        cmd: function() {
          if(grunt.config('echo.skipGESTMAGBuild')) {
            return 'echo "Nothing to do"';
          }
          return 'git rm share/webapps/GESTMAG/tags/*.tar.gz';
        }
      },
      gitAdd: {
        cmd: function() {
          var filesToAdd = [
            'package.json',
            'share/webapps/GESTMAG/package.json',
            'share/webapps/GESTMAG/app/scripts/directives/version/version.js'
          ];
          if(!grunt.config('echo.skipGESTMAGBuild')) {
            filesToAdd.push('share/webapps/GESTMAG/tags/' + grunt.config('version.upgradeVersionPackageJson.options.release') + '.tar.gz');
          }
          return 'git add ' + filesToAdd.join(' ');
        }
      },
      gitCommit: {
        cmd: function() {
          var msg = 'version upgrade';
          if(!grunt.config('echo.skipGESTMAGBuild')) {
            msg += ' e creazione pacchetto per tag ' + grunt.config('version.upgradeVersionPackageJson.options.release');
          }
          return 'git commit -m "' + msg + '"';
        }
      },
      gitPush: {
        cmd: function() {
          return 'git push';
        }
      },
      gitTag: {
        cmd: function() {
          return 'git tag -m "' + grunt.config('echo.make_current_tag_message').replace(/"/g, '\\"') + '" ' + grunt.config('version.upgradeVersionPackageJson.options.release');
        }
      },
      gitPushTag: {
        cmd: function() {
          return 'git push --tags';
        }
      }
    },

    'regex-replace': {
      version: {
        src: [
          'share/webapps/GESTMAG/app/scripts/directives/version/version.js'
        ],
        actions: [
          {
            search: '(.value\\(\'version\', \')([^\']*)(\'\\); // DON\'T EDIT THIS LINE MANUALLY)',
            replace: function(fullStr, pre, version, post) {
              return pre + grunt.config('version.upgradeVersionPackageJson.options.release') + post;
            }
          }
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-regex-replace');

  // build on "our own way"
  grunt.registerTask('check-wt', function() {
    var isWTDirty = function() {
      return !/^0/.test(exec('git status --porcelain | wc -l').toString());
    }
    if(isWTDirty()) {
      grunt.log.writeln(('WARNING: Git Working Tree dirty').white.bold);
      grunt.task.run('prompt:continueDespiteWTDirty');
    } else {
      grunt.log.writeln(('Git Working Tree clean').white.bold);
    }
  });

  grunt.registerTask('print-current-version', function() {
    grunt.log.writeln(('Current version is ' + pkg.version).white.bold);
  });

  grunt.registerTask('build-plus-plus', [
    'exec:gitStatus',
    'check-wt',
    'print-current-version',
    'prompt:configMakeTagAndVersionUpgrade',
    'version:upgradeVersionPackageJson',
    'regex-replace:version',
    'exec:gitRmOldTags',
    'exec:gruntBuildPlusPlusGESTMAG',
    'exec:gitAdd',
    'exec:gitCommit',
    'exec:gitTag',
    'exec:gitPush',
    'exec:gitPushTag'
  ]);

  grunt.registerTask('default', [
    'print-current-version'
  ]);

};
